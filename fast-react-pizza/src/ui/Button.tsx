import { ReactNode } from 'react';
import { Link } from 'react-router-dom';

interface IProps {
  children: ReactNode;
  disabled?: boolean;
  to?: string;
  type: 'small' | 'primary' | 'secondary' | 'round';
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
}
interface IStyles {
  [key: string]: string;
}
function Button({ children, disabled, to, type, onClick }: IProps) {
  const base =
    'inline-block rounded bg-amber-500  font-medium tracking-wider text-stone-800 transition-colors ease-linear hover:bg-amber-400 focus:outline-none focus:ring focus:ring-amber-400 focus:ring-offset-2 disabled:cursor-not-allowed ';

  const styles: IStyles = {
    primary: base + 'px-4 py-3 sm:px-5 sm:py-4',
    small: base + 'py-2 px-3 md:px-4 md:py-3 text-xs',
    secondary:
      'inline-block rounded  border-2 border-stone-300  font-medium tracking-wider text-stone-600 transition-colors ease-linear focus:outline-none focus:ring focus:ring-stone-600  hover:bg-stone-600 focus:ring-offset-2  hover:text-stone-300 focus:text-stone-300 disabled:cursor-not-allowed px-3 py-2 sm:px-4.5 sm:py-3.5',
    round: base + 'py-1.5 px-2.5 md:px-4.5 md:py-3.5 text-sm',
  };
  if (to)
    return (
      <Link to={to} className={styles[type]}>
        {children}
      </Link>
    );
  if (onClick) {
    return (
      <button onClick={onClick} disabled={disabled} className={styles[type]}>
        {children}
      </button>
    );
  }
  return (
    <button disabled={disabled} className={styles[type]}>
      {children}
    </button>
  );
}

export default Button;
