import { configureStore } from '@reduxjs/toolkit';
import userReducer, { IUserSlice } from './features/user/userSlice.ts';
import cartReducer, { ICartSlice } from './features/cart/cartSlice.ts';
export interface IRootState {
  user: IUserSlice;
  cart: ICartSlice;
}

const store = configureStore<IRootState>({
  reducer: {
    user: userReducer,
    cart: cartReducer,
  },
});
export default store;
