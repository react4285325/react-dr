import { useMapEvents } from 'react-leaflet';
import { useNavigate } from 'react-router-dom';

function DetectMapClick() {
  const navigate = useNavigate();
  useMapEvents({
    click: (event) => {
      navigate(`form?lat=${event.latlng.lat}&lng=${event.latlng.lng}`);
    },
  });
  return <div></div>;
}

export default DetectMapClick;
