// "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=0&longitude=0"

import { FormEvent, useEffect, useReducer, useState } from 'react';

import styles from './Form.module.css';
import Button from './shared/Button.tsx';
import BackButton from './shared/BackButton.tsx';
import { useUrlPosition } from '../hooks/useUrlPosition.ts';
import { convertToEmoji } from '../helper/fn.ts';
import Message from './shared/Message.tsx';
import Spinner from './shared/Spinner.tsx';
import { useApi } from '../hooks/useFetchApi.ts';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import useCities from '../hooks/useCities.ts';
import { ICitiesContext } from '../context/CititesContext.tsx';
import { useNavigate } from 'react-router-dom';
import { formReducer } from '../reducer/formReducer.ts';

const BASE_URL = `https://api.bigdatacloud.net/data/reverse-geocode-client?`;
const initialState = {
  country: '',
  cityName: '',
  note: '',
  date: new Date(),
  emoji: '',
};
function Form() {
  const [lat, lng] = useUrlPosition();
  const {
    isLoading: isLoadingGeocoding,
    data,
    error,
  } = useApi(`${BASE_URL}latitude=${lat}&longitude=${lng}`);
  const { createCity, isLoading } = useCities() as ICitiesContext;
  const navigate = useNavigate();
  const [{ date, emoji, note, country, cityName }, dispatch] = useReducer(
    formReducer,
    initialState
  );

  const [geoCodingError, setGeoCodingError] = useState('');
  useEffect(() => {
    setGeoCodingError(error);
    if (!data) return;
    if (!data.countryCode)
      setGeoCodingError('invalid city plz select another one');
    dispatch({ type: 'SET_CITY', payload: data.city || data.locality || '' });
    dispatch({ type: 'SET_COUNTRY', payload: data.countryName });
    dispatch({ type: 'SET_EMOJI', payload: convertToEmoji(data.countryCode) });
  }, [error, data, isLoadingGeocoding]);

  async function handleSubmit(e: FormEvent) {
    e.preventDefault();
    if (!cityName || !data) return;
    const newCity = {
      cityName,
      country,
      emoji,
      date: date.toISOString(),
      notes: note,
      position: {
        lat: +lat,
        lng: +lng,
      },
    };

    await createCity(newCity);
    navigate('/app/cities');
  }
  if (isLoadingGeocoding) return <Spinner />;
  if (!lat && !lat) return <Message message={'Start by clicking on Map'} />;
  if (geoCodingError) return <Message message={geoCodingError} />;
  return (
    <form
      className={`${styles.form} ${isLoading ? styles.loading : ''}`}
      onSubmit={handleSubmit}
    >
      <div className={styles.row}>
        <label htmlFor='cityName'>City name</label>
        <input
          id='cityName'
          onChange={(e) =>
            dispatch({ type: 'SET_CITY', payload: e.target.value })
          }
          value={cityName}
        />
        <span className={styles.flag}>{emoji}</span>
      </div>

      <div className={styles.row}>
        <label htmlFor='date'>When did you go to {cityName}?</label>
        <DatePicker
          id='date'
          selected={date}
          onChange={(dat: Date) => dispatch({ type: 'SET_DATE', payload: dat })}
          dateFormat={'dd/MM/yyyy'}
        />
      </div>

      <div className={styles.row}>
        <label htmlFor='notes'>Notes about your trip to {cityName}</label>
        <textarea
          id='notes'
          onChange={(e) =>
            dispatch({ type: 'SET_NOTE', payload: e.target.value })
          }
          value={note}
        />
      </div>

      <div className={styles.buttons}>
        <Button type={'primary'}>Add</Button>
        <BackButton />
      </div>
    </form>
  );
}

export default Form;
