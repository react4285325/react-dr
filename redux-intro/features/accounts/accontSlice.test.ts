import { describe, expect, it } from 'vitest';
import { deposit, payLoan, requestLoan, withdraw } from './acconuntSlice.ts';

describe('action for accounts', () => {
  it('should return action.type as accounts/withdraw', function () {
    expect(withdraw(500)).toStrictEqual({
      type: 'accounts/withdraw',
      payload: { amount: 500 },
    });
  });
  it('should return action.type as accounts/deposit and amount 1000', function () {
    expect(deposit(1000)).toStrictEqual({
      type: 'accounts/deposit',
      payload: { amount: 1000 },
    });
  });
  it('should return action.type as accounts/requestLoan and payload with amount and purpose', function () {
    expect(requestLoan(5050, 'for new pizza')).toStrictEqual({
      type: 'accounts/requestLoan',
      payload: { amount: 5050, purpose: 'for new pizza' },
    });
  });
  it('should return action.type as accounts/payLoan', function () {
    expect(payLoan()).toStrictEqual({
      type: 'accounts/payLoan',
    });
  });
});
