import { useSelector } from 'react-redux';
import { IRootState } from '../../store.ts';

function formatCurrency(value: number) {
  return new Intl.NumberFormat('en', {
    style: 'currency',
    currency: 'USD',
  }).format(value);
}

function BalanceDisplay() {
  const balance = useSelector((state: IRootState) => state.account.balance);
  return <div className='balance'>{formatCurrency(balance || 0)}</div>;
}

export default BalanceDisplay;
