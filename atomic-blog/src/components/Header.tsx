import Results from './Results.tsx';
import SearchPosts from './SearchPosts.tsx';
import usePosts from './hooks/usePosts.ts';

function Header() {
  const { onClearPost } = usePosts();

  return (
    <header>
      <h1>
        <span>⚛</span>The Atomic Blog
      </h1>
      <div>
        <Results />
        <SearchPosts />
        <button onClick={onClearPost}>Clear posts</button>
      </div>
    </header>
  );
}

export default Header;
