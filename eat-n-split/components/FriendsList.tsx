import { IFriends } from "./App.tsx";
import Friend from "./Friend.tsx";

interface Props {
  friends: IFriends[];
  onSelectFriend: (friend: IFriends) => void;
  selectedFriend: IFriends | null;
}
function FriendsList({ friends, onSelectFriend, selectedFriend }: Props) {
  return (
    <>
      {friends.map((friend) => (
        <Friend
          key={friend.id}
          friend={friend}
          onSelectFriend={onSelectFriend}
          selectedFriend={selectedFriend}
        />
      ))}
    </>
  );
}

export default FriendsList;
