import { content } from "./data.ts";
import Tabbed from "./components/Tabbed.tsx";

export default function App() {
  return (
    <div>
      <Tabbed content={content} />
    </div>
  );
}
