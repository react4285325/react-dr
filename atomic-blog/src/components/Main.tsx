import FormAddPost from './FormAddPost.tsx';
import Posts from './Posts.tsx';

function Main() {
  return (
    <main>
      <FormAddPost />
      <Posts />
    </main>
  );
}
export default Main;
