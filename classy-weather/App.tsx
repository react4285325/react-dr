import { Component } from 'react';
import Counter from './Counter.tsx';

class App extends Component {
  render() {
    return <Counter />;
  }
}

export default App;
