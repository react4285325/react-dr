import styles from './PageNav.module.css';
import { NavLink } from 'react-router-dom';
import Logo from './Logo.tsx';

const links = [
  { path: '/pricing', name: 'Pricing' },
  { path: '/product', name: 'Product' },
  { path: '/login', name: 'Login' },
];

function PageNav() {
  return (
    <nav className={styles.nav}>
      <Logo />
      <ul>
        {links.map((nav) => (
          <li key={nav.name}>
            <NavLink
              className={nav.name === 'Login' ? 'cta' : ''}
              to={nav.path}
            >
              {nav.name}
            </NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default PageNav;
