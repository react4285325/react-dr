import { useSearchParams } from 'react-router-dom';

export function useUrlPosition(): [string | number, string | number] {
  const [searchParams] = useSearchParams();
  const { lat = 0, lng = 0 } = Object.fromEntries(searchParams.entries());
  return [lat, lng];
}
