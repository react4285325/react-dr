import * as React from "react";
import * as ReactDom from "react-dom/client";
import "./index.css";
import App from "./App";

ReactDom.createRoot(document.querySelector("#root")).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
