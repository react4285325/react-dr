import { IFriends } from "./App.tsx";
import Button from "./Button.tsx";

interface Props {
  friend: IFriends;
  onSelectFriend: (friend: IFriends) => void;
  selectedFriend: IFriends | null;
}
function Friend({ friend, onSelectFriend, selectedFriend }: Props) {
  const isSelectedFriend = friend.id === selectedFriend?.id;
  return (
    <li className={isSelectedFriend ? "selected" : ""}>
      <img src={friend.image} alt={friend.name} />
      <h3>{friend.name}</h3>
      {friend.balance < 0 && (
        <p className='red'>
          You owe {friend.name} {Math.abs(friend.balance)}¥
        </p>
      )}{" "}
      {friend.balance > 0 && (
        <p className='green'>
          You {friend.name} owes you {Math.abs(friend.balance)}¥
        </p>
      )}{" "}
      {friend.balance === 0 && <p>You and {friend.name} are even</p>}
      <Button onClick={() => onSelectFriend(friend)}>
        {isSelectedFriend ? "Close" : "Select"}
      </Button>
    </li>
  );
}

export default Friend;
