function Loader() {
  return (
    <div className='bg-slate-20/30 absolute inset-0 flex items-center justify-center backdrop-blur-sm'>
      <div className={'loader'}>️</div>
    </div>
  );
}

export default Loader;
