import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import './index.css';
import App from './App.tsx';
import { QuizProvider } from './context/QuizContext.tsx';

ReactDOM.createRoot(document.querySelector('#root') as HTMLDivElement).render(
  <React.StrictMode>
    <QuizProvider>
      <App />
    </QuizProvider>
  </React.StrictMode>
);
