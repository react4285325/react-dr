import styles from './CountryList.module.css';
import Message from './shared/Message.tsx';
import Spinner from './shared/Spinner.tsx';
import CountryItem from './CountryItem.tsx';
import { ICity } from '../App.tsx';
import useCities from '../hooks/useCities.ts';
import { ICitiesContext } from '../context/CititesContext.tsx';

export interface ICountry {
  country: string;
  emoji: string;
}

function CityList() {
  const { isLoading, cities, error } = useCities() as ICitiesContext;
  if (isLoading) return <Spinner />;
  if (!cities.length)
    return (
      <Message message={'ADD your cities by clicking on a cities on you map'} />
    );
  if (error) return <Message message={error} />;
  const country: ICountry[] = cities.reduce((arr: ICountry[], city: ICity) => {
    if (!arr.map((el) => el.country).includes(city.country))
      return [...arr, { country: city.country, emoji: city.emoji }];
    else return arr;
  }, []);

  return (
    <ul className={styles.countryList}>
      {country.map((country) => (
        <CountryItem key={country.country} country={country} />
      ))}
    </ul>
  );
}

export default CityList;
