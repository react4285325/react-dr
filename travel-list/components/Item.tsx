import { Items } from "./App.tsx";
import { Fn } from "./PackingList.tsx";
interface Props extends Fn {
  item: Items;
}
function Item({ item, onDeleteItem, onToggleItem }: Props) {
  return (
    <li>
      <input
        type='checkbox'
        value={item.packed ? 1 : 0}
        onChange={() => onToggleItem(item.id)}
      />
      <span style={item.packed ? { textDecoration: "line-through" } : {}}>
        {item.quantity}
        {item.description}
      </span>
      <button onClick={() => onDeleteItem(item.id)}>❌</button>
    </li>
  );
}

export default Item;
