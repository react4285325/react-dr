import {
  Action,
  createSlice,
  PayloadAction,
  Slice,
  Dispatch,
} from '@reduxjs/toolkit';

export interface IAccountState {
  balance: number;
  loan: number;
  loanPurpose: string;
  isLoading: boolean;
}

const initialState: IAccountState = {
  balance: 0,
  loan: 0,
  loanPurpose: '',
  isLoading: false,
};

interface AccountSlice extends Slice<IAccountState> {
  reducers: {
    deposit: (
      state: IAccountState,
      action: PayloadAction<{ amount: number }>
    ) => void;
    withdraw: (
      state: IAccountState,
      action: PayloadAction<{ amount: number }>
    ) => void;
    requestLoan: (
      state: IAccountState,
      action: PayloadAction<{ amount: number; loanPurpose: string }>
    ) => void;
    payLoan: (state: IAccountState) => void;
    convertingCurrency: (state: IAccountState) => void;
  };
}
const accountSlice = createSlice<IAccountState, AccountSlice['reducers']>({
  name: 'account',
  initialState,
  reducers: {
    deposit(state, action) {
      state.balance += action.payload.amount;
      state.isLoading = false;
    },
    withdraw(state, action) {
      state.balance -= action.payload.amount;
    },
    requestLoan(state, action) {
      if (state.loan > 0) return;
      state.loan = action.payload.amount;
      state.loanPurpose = action.payload.loanPurpose;
      state.balance += action.payload.amount;
    },
    payLoan(state) {
      state.balance -= state.loan;
      state.loan = 0;
      state.loanPurpose = '';
    },
    convertingCurrency(state) {
      state.isLoading = true;
    },
  },
});

export function deposit(amount: number, currency: string) {
  if (currency === 'USD')
    return { type: 'account/deposit', payload: { amount } };
  return async function (dispatch: Dispatch<Action>) {
    dispatch({ type: 'account/convertingCurrency' });
    const res = await fetch(
      `https://api.frankfurter.app/latest?amount=${amount}&from=${currency}&to=USD`
    );
    const data = await res.json();
    const converted = data.rates.USD;
    if (!converted) throw new Error('invalid conversion rate');
    dispatch({
      type: 'account/deposit',
      payload: { amount: converted },
    });
  };
}

export const { payLoan, requestLoan, withdraw } = accountSlice.actions;
export default accountSlice.reducer;
