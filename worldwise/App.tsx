import { lazy, Suspense } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import {
  PRODUCT_PATH,
  CITIES_PATH,
  APP_PATH,
  PRICING_PATH,
  LOGIN_PATH,
  COUNTRIES_PATH,
  FORM_PATH,
} from './constants.ts';
import ProtectedRoute from './pages/ProtectedRoute.tsx';
import CitiesProvider from './context/CititesContext.tsx';
import { AuthProvider } from './context/AuthContext.tsx';

import CityList from './components/city/CityList.tsx';
import CountryList from './components/CountryList.tsx';
import City from './components/city/City.tsx';
import Form from './components/Form.tsx';
import SpinnerFullPage from './components/SpinnerFullPage.tsx';

const Homepage = lazy(() => import('./pages/Homepage.tsx'));
const Product = lazy(() => import('./pages/Product.tsx'));
const Pricing = lazy(() => import('./pages/Pricing.tsx'));
const PageNotFound = lazy(() => import('./pages/PageNotFound.tsx'));
const AppLayout = lazy(() => import('./pages/app/AppLayout.tsx'));
const Login = lazy(() => import('./pages/Login.tsx'));

export interface ICity {
  cityName: string;
  country: string;
  emoji: string;
  date: string;
  notes: string;
  position: {
    lat: number;
    lng: number;
  };
  id?: number;
}
function App() {
  return (
    <AuthProvider>
      <CitiesProvider>
        <BrowserRouter>
          <Suspense fallback={<SpinnerFullPage />}>
            <Routes>
              <Route index element={<Homepage />} />
              <Route path={PRODUCT_PATH} element={<Product />} />
              <Route path={PRICING_PATH} element={<Pricing />} />
              <Route path={LOGIN_PATH} element={<Login />} />
              <Route
                path={APP_PATH}
                element={
                  <ProtectedRoute>
                    <AppLayout />
                  </ProtectedRoute>
                }
              >
                <Route index element={<Navigate to={CITIES_PATH} replace />} />
                <Route path={CITIES_PATH} element={<CityList />} />
                <Route path={`${CITIES_PATH}/:id`} element={<City />} />
                <Route path={COUNTRIES_PATH} element={<CountryList />} />
                <Route path={FORM_PATH} element={<Form />} />
              </Route>

              <Route path={'*'} element={<PageNotFound />} />
            </Routes>
          </Suspense>
        </BrowserRouter>
      </CitiesProvider>
    </AuthProvider>
  );
}

export default App;
