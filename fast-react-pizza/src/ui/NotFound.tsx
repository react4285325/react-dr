import LinkButton from './LinkButton.tsx';

function NotFoundPage() {
  return (
    <>
      <div className='h-full w-full text-center'>
        <h1>404 - Page Not Found</h1>
        <p>The page you are looking for does not exist.</p>
        <LinkButton to={'/'}>Go HOME</LinkButton>
      </div>
    </>
  );
}

export default NotFoundPage;
