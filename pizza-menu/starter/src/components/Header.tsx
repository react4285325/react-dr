const Header = () => {
  return (
    <header className={"header"}>
      <h1>Vintage Pizza Ltd</h1>
    </header>
  );
};

export default Header;
