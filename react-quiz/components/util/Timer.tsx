import { useEffect } from 'react';
import { useQuiz } from '../../hooks/useQuiz.ts';
import { IQuizContext } from '../../context/QuizContext.tsx';

function Timer() {
  const { dispatch, secondsRemaining } = useQuiz() as IQuizContext;
  useEffect(() => {
    const id = setInterval(() => {
      dispatch({ type: 'TICK' });
    }, 1000);
    return () => window.clearInterval(id);
  }, [dispatch]);

  return <div className={'timer'}>{secondsRemaining}</div>;
}

export default Timer;
