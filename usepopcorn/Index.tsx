import * as React from "react";
import * as ReactDom from "react-dom/client";
import "./index.css";
import App from "./components/App.tsx";
import StarRating from "./components/shared/StarRating.tsx";
import { useState } from "react";
/*function Test() {
  const [moveRating, setMoveRating] = useState(0);

  return (
    <div>
      <StarRating
        maxRating={10}
        defaultRating={1}
        color={"green"}
        onSetRating={setMoveRating}
      />
      <p>ratings {moveRating}</p>
    </div>
  );
}*/

ReactDom.createRoot(document.querySelector("#root") as HTMLDivElement).render(
  <React.StrictMode>
    <App />

    {/*<Test />*/}
  </React.StrictMode>
);
