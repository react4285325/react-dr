import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';

export interface ICustomerState {
  fullName: string;
  nationalID: string;
  createAt: string;
}

export interface ICustomerUpdate {
  fullName: string;
}

const initialStateCustomer: ICustomerState = {
  fullName: '',
  nationalID: '',
  createAt: '',
};

interface CustomerSlice extends Slice<ICustomerState> {
  reducers: {
    createCustomer: {
      prepare(
        fullName: string,
        nationalID: string
      ): {
        payload: { nationalID: string; fullName: string; createAt: string };
      };
      reducer: (
        state: ICustomerState,
        action: PayloadAction<{
          fullName: string;
          nationalID: string;
          createAt: string;
        }>
      ) => void;
    };
    updateName: (
      state: ICustomerState,
      action: PayloadAction<ICustomerState>
    ) => void;
  };
}

const customerSlice = createSlice<ICustomerState, CustomerSlice['reducers']>({
  name: 'customer',
  initialState: initialStateCustomer,
  reducers: {
    createCustomer: {
      prepare(fullName: string, nationalID: string) {
        return {
          payload: {
            fullName,
            nationalID,
            createAt: new Date().toISOString(),
          },
        };
      },
      reducer: (
        state,
        action: PayloadAction<{
          fullName: string;
          nationalID: string;
          createAt: string;
        }>
      ) => {
        state.fullName = action.payload.fullName;
        state.nationalID = action.payload.nationalID;
        state.createAt = action.payload.createAt;
      },
    },
    updateName: (state, action) => {
      state.fullName = action.payload.fullName;
    },
  },
});

export const { createCustomer, updateName } = customerSlice.actions;
export default customerSlice.reducer;
