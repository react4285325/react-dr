import Button from "./Button.tsx";

function FormAddFriend() {
  return (
    <form className={"form-add-friend"}>
      <label htmlFor='name'>😮 Friend Name</label>
      <input id='name' type={"text"} />
      <label htmlFor='image'>👧 Image URL</label>
      <input id='image' type={"text"} />
      <Button>Add</Button>
    </form>
  );
}

export default FormAddFriend;
