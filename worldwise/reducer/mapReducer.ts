import { ICity } from '../App.tsx';

export interface IState {
  cities: ICity[];
  isLoading: boolean;
  currentCity: ICity;
  error: string;
}
export interface IAction {
  type:
    | 'loading'
    | 'cities/loaded'
    | 'city/created'
    | 'cities/deleted'
    | 'rejected'
    | 'city/loaded';
  payload?: string | boolean | ICity[] | ICity;
}

export function reducer(state: IState, action: IAction): IState {
  switch (action.type) {
    case 'loading':
      return { ...state, isLoading: true };
    case 'cities/loaded':
      return {
        ...state,
        isLoading: false,
        cities: action.payload as ICity[],
      };
    case 'city/loaded':
      return {
        ...state,
        isLoading: false,
        currentCity: action.payload as ICity,
      };
    case 'city/created':
      return {
        ...state,
        isLoading: false,
        cities: [...state.cities, action.payload as ICity],
        currentCity: action.payload as ICity,
      };
    case 'cities/deleted':
      return {
        ...state,
        isLoading: false,
        cities: state.cities.filter((city) => city.id !== action.payload),
      };

    case 'rejected':
      return {
        ...state,
        isLoading: false,
        error: action.payload as string,
      };

    default:
      throw new Error('invalid action type');
  }
}
