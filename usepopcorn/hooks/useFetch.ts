import { useEffect, useState } from "react";
import { MOVIE_API_KEY } from "../../env.local.ts";
import { IMovieData } from "../data.ts";

function useFetch(query: string): [boolean, string, IMovieData[]] {
  const [isLoading, setIsLoading] = useState(false);
  const [movies, setMovies] = useState<IMovieData[]>([]);
  const [error, setError] = useState("");

  useEffect(() => {
    // callback?.();
    const controller = new AbortController();
    (async function () {
      try {
        setMovies([]);
        setError("");
        const res = await fetch(
          `https://www.omdbapi.com/?apikey=${MOVIE_API_KEY}&s=${query}`,
          { signal: controller.signal }
        );
        if (!res.ok) {
          throw new Error("something went wrong");
        }

        const data = await res.json();
        if (data.Response === "False") throw new Error("No Movies Found");
        setMovies(data.Search);
      } catch (e) {
        if ((e as Error).name !== "AbortError") setError((e as Error).message);
      } finally {
        setIsLoading(false);
        setError("");
      }
    })();
    return () => controller.abort("too many req");
  }, [query]);
  return [isLoading, error, movies];
}
export default useFetch;
