import Pizza from "./Pizza";
export function Menu({ pizza }) {
  return (
    <div className={"menu"}>
      <h2>Our Menu</h2>

      {pizza ? (
        <>
          <p>Lorem ipsum dolor sit amet.</p> <Pizza pizza={pizza} />
        </>
      ) : (
        <p>nothing found :(</p>
      )}
    </div>
  );
}
