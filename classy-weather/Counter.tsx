import { ChangeEvent, Component } from "react";
import Weather from "./Weather.tsx";
import Input from "./Input.tsx";
import { convertToFlag } from "./starter.ts";

export interface IWeathers {
  time: string[];
  weathercode: number[];
  temperature_2m_max: number[];
  temperature_2m_min: number[];
}

interface AppData {
  location: string;
  isLoading: boolean;
  displayLocation: string;
  weather: IWeathers;
}
class Counter extends Component<string, AppData> {
  state = {
    location: "",
    isLoading: false,
    displayLocation: "",
    weather: {
      time: [],
      weathercode: [],
      temperature_2m_max: [],
      temperature_2m_min: [],
    },
  };

  handleSetLocation = (e: ChangeEvent<HTMLInputElement>) => {
    this.setState((curState) => {
      return { ...curState, location: e.target.value };
    });
  };
  componentDidMount(): void {
    this.setState({ location: localStorage.getItem("location") || "" });
  }

  async componentDidUpdate(
    _: Readonly<string>,
    prevState: Readonly<AppData>
  ): Promise<void> {
    if (this.state.location !== prevState.location) {
      await this.handleFetchWeather();
      localStorage.setItem("location", this.state.location);
    }
  }

  handleFetchWeather = async () => {
    if (this.state.location.length < 2) return;
    try {
      this.setState({ isLoading: true });
      // 1) Getting location (geocoding)
      const geoRes = await fetch(
        `https://geocoding-api.open-meteo.com/v1/search?name=${this.state.location}`
      );
      const geoData = await geoRes.json();

      if (!geoData.results) throw new Error("Location not found");

      const { latitude, longitude, timezone, name, country_code } =
        geoData.results.at(0);
      this.setState({
        displayLocation: `${name} ${convertToFlag(country_code)}`,
      });

      // 2) Getting actual weather
      const weatherRes = await fetch(
        `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&timezone=${timezone}&daily=weathercode,temperature_2m_max,temperature_2m_min`
      );
      const weatherData = await weatherRes.json();
      this.setState({ weather: weatherData.daily });
    } catch (err) {
      console.error(err);
    } finally {
      this.setState({ isLoading: false });
    }
  };

  render() {
    return (
      <div className={"app"}>
        <h1>Classy Weather</h1>
        <div>
          <Input
            location={this.state.location}
            onChangeLocation={this.handleSetLocation}
          />
        </div>
        {this.state.isLoading && <p className={"loader"}>Loading..</p>}
        {this.state.weather.weathercode && (
          <Weather
            weather={this.state.weather}
            location={this.state.displayLocation}
          />
        )}
      </div>
    );
  }
}

export default Counter;
