import LinkButton from '../../ui/LinkButton.tsx';

function EmptyCart() {
  return (
    <div>
      <LinkButton to={'/menu'}>&larr; Back to menu</LinkButton>

      <p className={' px-2 py-3  font-semibold'}>
        Your cart is still empty. Start adding some pizzas :)
      </p>
    </div>
  );
}

export default EmptyCart;
