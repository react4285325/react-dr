import { useMap } from 'react-leaflet';
import { LatLngExpression } from 'leaflet';

function ChangeCenter({ position }: { position: LatLngExpression }) {
  const map = useMap();
  map.setView(position, 8);
  return null;
}
export default ChangeCenter;
