import { ReactNode } from "react";
import Logo from "./Logo.tsx";

interface IProps {
  children: ReactNode;
}
function Navbar({ children }: IProps) {
  return (
    <nav className='nav-bar'>
      <Logo />

      {children}
    </nav>
  );
}

export default Navbar;
