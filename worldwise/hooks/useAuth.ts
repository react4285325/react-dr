import { AuthContext } from '../context/AuthContext.tsx';
import { useContext } from 'react';

export function useAuth() {
  const context = useContext(AuthContext);
  if (context === undefined)
    throw new Error('the Auth Provider called outside Auth context');
  return context;
}
