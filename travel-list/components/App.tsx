import Logo from "./Logo";
import Form from "./Form";
import Stats from "./Stats";
import PackingList from "./PackingList";
import { useState } from "react";
export interface Items {
  description: string;
  quantity: number;
  id: number;
  packed: boolean;
}
function App() {
  const [items, setItems] = useState<Items[]>([]);
  const handleAddItems = (item: Items) =>
    setItems((prevState) => [...prevState, item]);

  const handleDelItem = (id: number) => {
    setItems((prevState) => prevState.filter((item) => item.id !== id));
  };

  const handleToggleItem = (id: number) =>
    setItems((prevState) =>
      prevState.map((item) =>
        item.id === id ? { ...item, packed: !item.packed } : item
      )
    );

  const handleClearList = () => {
    if (!items.length) return;
    const confirmed = window.confirm("are your sure you want to delete all");
    if (confirmed) setItems([]);
  };
  return (
    <>
      <Logo />
      <Form onAddItems={handleAddItems} />
      <PackingList
        items={items}
        onDeleteItem={handleDelItem}
        onToggleItem={handleToggleItem}
        onClearList={handleClearList}
      />
      <Stats items={items} />
    </>
  );
}

export default App;
