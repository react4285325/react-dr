const PRODUCT_PATH = 'product';
const PRICING_PATH = 'pricing';
const LOGIN_PATH = 'login';
const APP_PATH = 'app';
const CITIES_PATH = 'cities';
const COUNTRIES_PATH = 'countries';
const FORM_PATH = 'form';
export {
  CITIES_PATH,
  COUNTRIES_PATH,
  LOGIN_PATH,
  APP_PATH,
  PRICING_PATH,
  PRODUCT_PATH,
  FORM_PATH,
};
