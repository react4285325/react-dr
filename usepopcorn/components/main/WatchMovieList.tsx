import { IWatchData } from "../../data.ts";
import WatchMovie from "./WatchMovie.tsx";

interface IProps {
  watched: IWatchData[];
  onDeleteWatched: (id: string) => void;
}
function WatchMovieList({ watched, onDeleteWatched }: IProps) {
  return (
    <ul className='list'>
      {watched.map((movie) => (
        <WatchMovie
          onDeleteWatched={onDeleteWatched}
          movie={movie}
          key={movie.imdbId}
        />
      ))}
    </ul>
  );
}

export default WatchMovieList;
