import Header from './Header.tsx';
import CartOverview from '../features/cart/CartOverview.tsx';
import { Outlet, useNavigation } from 'react-router-dom';
import Loader from './Loader.tsx';

function AppLayout() {
  const navigation = useNavigation();
  const isLoading = navigation.state === 'loading';

  return (
    <div className='grid h-screen grid-rows-[auto_1fr_auto]'>
      {isLoading && <Loader />}
      <Header />
      <main className='mx-full max-w-3xl overflow-scroll'>
        <Outlet />
      </main>

      <CartOverview />
    </div>
  );
}

export default AppLayout;
