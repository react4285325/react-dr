import { useSelector } from 'react-redux';
import { IRootState } from '../../store.ts';

function Customer() {
  const customer = useSelector((store: IRootState) => store.customer);
  return <h2>👋 {customer?.fullName}</h2>;
}

export default Customer;
