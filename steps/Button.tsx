import { ReactNode } from "react";

interface Props {
  bgColor: string;
  children: ReactNode;
  clickHandler: () => void;
  color: string;
}

function Button({ color, bgColor, clickHandler, children }: Props) {
  return (
    <>
      <button
        onClick={clickHandler}
        style={{ backgroundColor: bgColor, color }}
      >
        {children}
      </button>
    </>
  );
}

export default Button;
