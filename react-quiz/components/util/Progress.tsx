import { useQuiz } from '../../hooks/useQuiz.ts';
import { IQuizContext } from '../../context/QuizContext.tsx';

function Progress() {
  const { index, numQuestion, points, maxPoint, answer } =
    useQuiz() as IQuizContext;
  return (
    <header className='progress'>
      <progress max={numQuestion} value={index + Number(answer !== null)} />
      <p>
        Question{' '}
        <strong>
          {index}/{numQuestion}
        </strong>
      </p>
      <p>
        <strong>
          {points}/{maxPoint}
        </strong>
      </p>
    </header>
  );
}

export default Progress;
