import { configureStore } from '@reduxjs/toolkit';
import accountReducer, {
  IAccountState,
} from './features/accounts/acconuntSlice.ts';
import customerReducer, {
  ICustomerState,
} from './features/customers/customerSlice.ts';

export interface IRootState {
  account: IAccountState;
  customer: ICustomerState;
}

const store = configureStore<IRootState>({
  reducer: {
    account: accountReducer,
    customer: customerReducer,
  },
});

export default store;
