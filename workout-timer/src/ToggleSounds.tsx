import { Dispatch, SetStateAction, memo } from 'react';

interface IProps {
  allowSound: boolean;
  setAllowSound: Dispatch<SetStateAction<boolean>>;
}

function ToggleSounds({ allowSound, setAllowSound }: IProps) {
  return (
    <button
      className='btn-sound'
      onClick={() => setAllowSound((allow) => !allow)}
    >
      {allowSound ? '🔈' : '🔇'}
    </button>
  );
}

export default memo(ToggleSounds);
