interface IProps {
  footer: string;
  copyright: string;
}
function Footer({ footer, copyright }: IProps) {
  return (
    <footer className={footer}>
      <p className={copyright}>
        All rights Reserved &copy; {new Date().getFullYear()} by me
      </p>
    </footer>
  );
}

export default Footer;
