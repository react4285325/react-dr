import { useState } from "react";
import Tab from "./Tab.tsx";
import TabContent from "./TabContent.tsx";
import DifferentContent from "./DiffContent.tsx";
import { IContent } from "../data.ts";
interface IProps {
  content: IContent[];
}
function Tabbed({ content }: IProps) {
  const [activeTab, setActiveTab] = useState(0);
  const arr = [0, 1, 2, 3];

  return (
    <div>
      <div className='tabs'>
        {arr.map((v) => (
          <Tab key={v} num={v} activeTab={activeTab} onClick={setActiveTab} />
        ))}
      </div>

      {activeTab <= 2 ? (
        <TabContent key={content[activeTab].id} item={content[activeTab]} />
      ) : (
        <DifferentContent />
      )}
    </div>
  );
}

export default Tabbed;
