import { useQuiz } from '../hooks/useQuiz.ts';
import { IQuizContext } from '../context/QuizContext.tsx';

function Options() {
  const { questions, answer, index, dispatch } = useQuiz() as IQuizContext;
  return (
    <div className='options'>
      {questions[index].options.map((opt, i) => (
        <button
          key={opt}
          className={`btn btn-option ${i === answer ? 'answer' : ''} ${
            answer !== null
              ? i === questions[index].correctOption
                ? 'correct'
                : 'wrong'
              : ''
          }`}
          onClick={() => dispatch({ type: 'NEW_ANSWER', payload: i })}
          disabled={answer !== null}
        >
          {opt}
        </button>
      ))}
    </div>
  );
}

export default Options;
