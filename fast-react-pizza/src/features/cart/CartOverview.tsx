import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getTotalCartPrice, getTotalCartQuantity } from './cartSlice.ts';
import { formatCurrency } from '../../utility/helpers.ts';

function CartOverview() {
  const totalCartQuantity = useSelector(getTotalCartQuantity);
  const totalCartPrice = useSelector(getTotalCartPrice);
  if (!totalCartPrice) return false;
  return (
    <div className='flex items-center justify-between bg-stone-800 px-4 py-4 text-center text-sm uppercase text-stone-300 sm:px-6 md:text-base'>
      <p className=' nt-bold  space-x-3 text-stone-200 sm:space-x-6'>
        <span>{totalCartQuantity} pizzas</span>
        <span>{formatCurrency(totalCartPrice)}</span>
      </p>
      <Link to='/cart'>Open cart &rarr;</Link>
    </div>
  );
}

export default CartOverview;
