const Footer = () => {
  const hour = new Date().getHours();
  const OPEN_HOUR = 12;
  const CLOSING_HOUR = 22;
  const isOpen = hour >= OPEN_HOUR && hour <= CLOSING_HOUR;
  return (
    <footer className={"footer"}>
      {isOpen && (
        <div className={"order"}>
          <p>:) Not available in your country</p>
          <button className={"btn"}>Notify</button>
        </div>
      )}
    </footer>
  );
};

export default Footer;
