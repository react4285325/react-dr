import { IAuth, IUser } from '../context/AuthContext.tsx';

interface IAuthAction {
  type: 'LOGIN' | 'LOGOUT';
  payload?: IUser | boolean;
}
export function authReducer(state: IAuth, action: IAuthAction): IAuth {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        user: action.payload as IUser,
        isAuthenticated: true,
      };
    case 'LOGOUT':
      return { ...state, user: null, isAuthenticated: false };
    default:
      throw new Error('invalid actionType 💀');
  }
}
