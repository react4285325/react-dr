import Item from "./Item";
import { Items } from "./App.tsx";
import { useState } from "react";

export interface Fn {
  onDeleteItem: (id: number) => void;
  onToggleItem: (id: number) => void;
}
interface Props extends Fn {
  items: Items[];
  onClearList: () => void;
}
function PackingList({
  items,
  onDeleteItem,
  onToggleItem,
  onClearList,
}: Props) {
  const [sortBy, setSortBy] = useState("input");

  let sortedItems;
  if (sortBy === "input") sortedItems = items;
  if (sortBy === "description") {
    sortedItems = items
      .slice()
      .sort((a, b) => a.description.localeCompare(b.description));
  }
  if (sortBy === "packed")
    sortedItems = items.slice().sort((a, b) => +a.packed - +b.packed);

  return (
    <div className={"list"}>
      <ul>
        {sortedItems?.map((item) => (
          <Item
            item={item}
            key={item.id}
            onDeleteItem={onDeleteItem}
            onToggleItem={onToggleItem}
          />
        ))}
      </ul>

      <div className={"actions"}>
        <select
          value={sortBy}
          onChange={(event) => setSortBy(event.target.value)}
        >
          <option value={"input"}>Sort by input order</option>
          <option value={"description"}>Sort by description </option>
          <option value={"packed"}>Sort by packed status</option>
        </select>
        <button onClick={onClearList}>Clear List</button>
      </div>
    </div>
  );
}

export default PackingList;
