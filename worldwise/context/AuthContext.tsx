import { createContext, ReactNode, useReducer } from 'react';
import { authReducer } from '../reducer/authReducer.ts';
interface IProps {
  children: ReactNode;
}
export interface IUser {
  email: string;
  password: string;
  name: string;
  avatar: string;
}
export interface IAuth {
  user: null | IUser;
  isAuthenticated: boolean;
}
export interface IAuthProvider {
  user: IUser | null;
  isAuthenticated: boolean;
  login: (email: string, password: string) => void;
  logout: () => void;
}
export const AuthContext = createContext<IAuthProvider | null>(null);

const initialState = {
  user: null,
  isAuthenticated: false,
};

const FAKE_USER = {
  name: 'Jack',
  email: 'email@nomail.com',
  password: '10liam',
  avatar: 'https://i.pravatar.cc/100?u=zz',
};
export function AuthProvider({ children }: IProps) {
  const [{ user, isAuthenticated }, dispatch] = useReducer(
    authReducer,
    initialState
  );
  function login(email: string, password: string) {
    if (email === FAKE_USER.email && password === FAKE_USER.password) {
      dispatch({ type: 'LOGIN', payload: FAKE_USER });
    }
  }
  function logout() {
    dispatch({ type: 'LOGOUT' });
  }
  return (
    <AuthContext.Provider value={{ user, isAuthenticated, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
}
