interface IProps {
  num: number;
  activeTab: number;
  onClick: (num: number) => void;
}
function Tab({ num, activeTab, onClick }: IProps) {
  return (
    <button
      className={activeTab === num ? "tab active" : "tab"}
      onClick={() => onClick(num)}
    >
      Tab {num + 1}
    </button>
  );
}
export default Tab;
