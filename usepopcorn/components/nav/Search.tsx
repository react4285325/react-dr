import { useRef } from "react";
import useKey from "../../hooks/useKey.ts";

interface IProps {
  query: string;
  setQuery: (q: string) => void;
}
function Search({ query, setQuery }: IProps) {
  const inputElement = useRef<HTMLInputElement>(null);

  useKey("Enter", () => {
    if (document.activeElement === inputElement.current) return;
    inputElement.current?.focus();
    setQuery("");
  });
  return (
    <>
      <input
        ref={inputElement}
        className='search'
        type='text'
        placeholder='Search movies...'
        value={query}
        onChange={(e) => setQuery(e.target.value)}
      />
    </>
  );
}

export default Search;
