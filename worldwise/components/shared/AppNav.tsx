import style from "./AppNav.module.css";
import { NavLink } from "react-router-dom";
const nav = ["cities", "countries"];
function AppNav() {
  return (
    <nav className={style.nav}>
      <ul>
        {nav.map((nav) => (
          <li key={nav}>
            <NavLink to={nav}>{nav}</NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default AppNav;
