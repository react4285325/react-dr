import Order from './Order.tsx';
import Button from '../../ui/Button.tsx';
import { ActionFunctionArgs, useFetcher } from 'react-router-dom';
import { updateOrder } from '../../services/apiRestaurant.ts';

interface IProps {
  order: Order;
}
function UpdateOrder({ order }: IProps) {
  const fetcher = useFetcher();
  return (
    <fetcher.Form method='PATCH' className={'text-right'}>
      <Button type={'primary'}>Make Priority</Button>
    </fetcher.Form>
  );
}

export default UpdateOrder;

export async function action({ _, params }: ActionFunctionArgs) {
  const { orderId } = params;
  const data = { priority: true };
  if (orderId) await updateOrder(orderId, data);
  return null;
}
