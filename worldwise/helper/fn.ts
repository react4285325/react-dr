const EMOJI_OFFSET = 127397;

export const formatDate = (date: string | null) =>
  new Intl.DateTimeFormat('en', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
    weekday: 'long',
  }).format(new Date(date || new Date()));

export function convertToEmoji(countryCode: string) {
  const codePoints = countryCode
    .toUpperCase()
    .split('')
    .map((char) => EMOJI_OFFSET + char.charCodeAt(0));
  return String.fromCodePoint(...codePoints);
}
