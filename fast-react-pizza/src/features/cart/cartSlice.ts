import { createSlice } from '@reduxjs/toolkit';
import { IRootState } from '../../store.ts';

interface IPizza {
  pizzaId: number;
  name: string;
  quantity: number;
  unitPrice: number;
  totalPrice: number;
}
export interface ICartSlice {
  cart: IPizza[];
}
const initialState: ICartSlice = {
  cart: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem(state, action) {
      state.cart.push(action.payload);
    },
    removeItem(state, action) {
      const id = action.payload;
      state.cart = state.cart.filter((item) => item.pizzaId !== id);
    },
    increaseItemQuantity(state, action) {
      const item = state.cart.find((item) => item.pizzaId === action.payload);
      if (item) {
        item.quantity++;
        item.totalPrice = item.quantity * item.unitPrice;
      }
    },
    decreaseItemQuantity(state, action) {
      const item = state.cart.find((item) => item.pizzaId === action.payload);
      if (item) {
        item.quantity--;
        item.totalPrice = item.quantity * item.unitPrice;
      }
      if (item?.quantity === 0)
        cartSlice.caseReducers.removeItem(state, action);
    },
    clearCart(state) {
      state.cart = [];
    },
  },
});

export const {
  addItem,
  clearCart,
  decreaseItemQuantity,
  increaseItemQuantity,
  removeItem,
} = cartSlice.actions;
export default cartSlice.reducer;

export const getUsername = (state: IRootState) => state.user.username;
export const getCart = (state: IRootState) => state.cart.cart;
export const getTotalCartQuantity = (state: IRootState) =>
  state.cart.cart.reduce((acc, currentValue) => acc + currentValue.quantity, 0);

export const getTotalCartPrice = (state: IRootState): number => {
  return state.cart.cart.reduce(
    (acc, currentValue) => acc + currentValue.totalPrice,
    0
  );
};
export const getCurrentQuantityById = (id: number) => (state: IRootState) => {
  return state.cart.cart.find((item) => item.pizzaId === id)?.quantity ?? 0;
};
