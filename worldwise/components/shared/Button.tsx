import { MouseEventHandler, ReactNode } from 'react';
import styles from './Button.module.css';

interface IProps<T extends EventTarget = HTMLButtonElement> {
  children: ReactNode;
  onClick?: MouseEventHandler<T>;
  type: 'primary' | 'back' | 'position';
}

function Button({ children, onClick, type }: IProps) {
  return (
    <button className={`${styles.btn} ${styles[type]}`} onClick={onClick}>
      {children}
    </button>
  );
}

export default Button;
