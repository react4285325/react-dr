import styles from './Map.module.css';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';
import { useEffect, useState } from 'react';
import { LatLngExpression } from 'leaflet';
import useCities from '../../hooks/useCities.ts';
import ChangeCenter from './ChangeMapCenter.tsx';
import DetectMapClick from './DetectMapClick.tsx';
import { useGeolocation } from '../../hooks/useGeolocation.ts';
import Button from '../shared/Button.tsx';
import { useUrlPosition } from '../../hooks/useUrlPosition.ts';
import { ICitiesContext } from '../../context/CititesContext.tsx';
import Message from '../shared/Message.tsx';
import Spinner from '../shared/Spinner.tsx';

function Map() {
  const { cities, error, isLoading } = useCities() as ICitiesContext;
  const [mapPosition, setMapPosition] = useState<LatLngExpression>([
    41.8902, 12.4922,
  ]);
  const {
    isLoading: isLoadingPos,
    position: geoPosition,
    getPosition,
  } = useGeolocation({ lat: 41.8902, lng: 12.4933 });
  const [lat, lng] = useUrlPosition();

  useEffect(() => {
    if (lat && lng) setMapPosition([+lat, +lng]);
  }, [lat, lng]);

  useEffect(() => {
    if (geoPosition) setMapPosition(geoPosition);
  }, [geoPosition]);

  if (isLoading) return <Spinner />;
  if (error) return <Message message={error} />;
  return (
    <div className={styles.mapContainer}>
      {geoPosition && (
        <Button type={'position'} onClick={getPosition}>
          {isLoadingPos ? 'loading..' : 'use your position'}
        </Button>
      )}
      <MapContainer
        center={mapPosition}
        className={styles.map}
        zoom={8}
        scrollWheelZoom={true}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'
        />
        {cities.map((city) => (
          <Marker key={city.id} position={city.position}>
            <Popup key={city.id}>
              <span>{city.emoji}</span> <span>{city.cityName}</span>
            </Popup>
          </Marker>
        ))}
        <ChangeCenter position={mapPosition} />
        <DetectMapClick />
      </MapContainer>
    </div>
  );
}

export default Map;
