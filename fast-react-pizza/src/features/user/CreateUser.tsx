import { useState } from 'react';
import Button from '../../ui/Button.tsx';
import { useDispatch } from 'react-redux';
import { updateName } from './userSlice.ts';
import { useNavigate } from 'react-router-dom';

function CreateUser() {
  const [username, setUsername] = useState('');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    dispatch(updateName(username));
    navigate('/menu');
  }

  return (
    <form onSubmit={handleSubmit}>
      <p className='mb-3 text-sm text-stone-500 md:text-base'>
        👋 Welcome! Please start by telling us your name:
      </p>

      <input
        type='text'
        placeholder='Your full name'
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        className='input w-62 mb-8'
      />

      {username !== '' && (
        <div>
          <Button type={'primary'} disabled={false}>
            Start ordering
          </Button>
        </div>
      )}
    </form>
  );
}

export default CreateUser;
