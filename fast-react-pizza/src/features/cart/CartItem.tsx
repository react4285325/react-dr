import { formatCurrency } from '../../utility/helpers.ts';
import { ICart } from './Cart.tsx';
import DeleteItem from './DeleteItem.tsx';
import UpdateItemQuantity from './UpdateItemQuantity.tsx';

interface IProps {
  item: ICart;
}
function CartItem({ item }: IProps) {
  const { name, quantity, totalPrice, pizzaId } = item;

  return (
    <li className='py-3 sm:flex sm:items-center sm:justify-between'>
      <p>
        {quantity}&times; {name}
      </p>
      <div className='mt-1 flex items-center justify-between sm:mt-0 sm:gap-5'>
        <p className={'text-sm font-bold'}>{formatCurrency(totalPrice)}</p>
        <UpdateItemQuantity currentQuantity={quantity} pizzaId={pizzaId} />
        <DeleteItem pizzaId={pizzaId} />
      </div>
    </li>
  );
}

export default CartItem;
