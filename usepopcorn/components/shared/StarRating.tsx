import Star from "./Star.tsx";
import { useState } from "react";

const containerStyle = {
  display: "flex",
  alignItems: "center",
  gap: "1rem",
};
const starContainerStyle = {
  display: "flex",
  gap: ".2rem",
};

interface IProps {
  maxRating: number;
  color?: string;
  size?: string;
  messages?: string[];
  className?: string;
  defaultRating: number;
  onSetRating: (rating: number) => void;
}
function StarRating({
  maxRating = 5,
  color = "gold",
  size = "20",
  messages = [],
  className = "",
  defaultRating = 0,
  onSetRating,
}: IProps) {
  const [rating, setRating] = useState(defaultRating);
  const [tempRating, setTempRating] = useState(0);
  const handleSetRating = (i: number) => {
    setRating(i + 1);
    onSetRating(i + 1);
  };
  const handleHoverIn = (rating: number) => setTempRating(rating);
  const handleHoverOut = () => setTempRating(0);

  const textStyle = {
    lineHeight: 1,
    margin: 0,
    color,
    fontSize: `${size}px`,
  };

  return (
    <div style={containerStyle} className={className}>
      <div style={starContainerStyle}>
        {Array.from({ length: maxRating }, (_, i) => (
          <Star
            style={{ color, size }}
            key={i}
            onRate={() => handleSetRating(i)}
            full={tempRating ? tempRating >= i + 1 : rating >= i + 1}
            onHoverIn={() => handleHoverIn(i + 1)}
            onHoverOut={handleHoverOut}
          />
        ))}
      </div>
      <p style={textStyle}>
        {messages?.length === maxRating
          ? messages[tempRating ? tempRating - 1 : rating - 1]
          : tempRating || rating || ""}
      </p>
    </div>
  );
}

export default StarRating;
