import { useQuiz } from '../hooks/useQuiz.ts';
import { IQuizContext } from '../context/QuizContext.tsx';

function getEmojiByPercentage(percentage: number) {
  if (percentage >= 90) {
    return '😃'; // Very happy face
  } else if (percentage >= 70) {
    return '🙂'; // Happy face
  } else if (percentage >= 50) {
    return '😐'; // Neutral face
  } else if (percentage >= 30) {
    return '😕'; // Sad face
  } else {
    return '😢'; // Very sad face
  }
}

function FinishScreen() {
  const { points, maxPoint, highScore, dispatch } = useQuiz() as IQuizContext;
  const percentage = (points / maxPoint) * 100;
  const emoji = getEmojiByPercentage(percentage);
  return (
    <>
      <p className='result'>
        ^_~ You Scored{' '}
        <strong>
          {points} out of {maxPoint} ({Math.ceil(percentage)}%) {emoji}
        </strong>
      </p>
      <p className={'highscore'}> HighScore: {highScore} points </p>
      <button
        className={'btn btn-ui'}
        onClick={() => dispatch({ type: 'RESET' })}
      >
        RestartQuiz
      </button>
    </>
  );
}

export default FinishScreen;
