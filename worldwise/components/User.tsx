import styles from './User.module.css';
import { useAuth } from '../hooks/useAuth.ts';
import { IAuthProvider } from '../context/AuthContext.tsx';
import { useNavigate } from 'react-router-dom';

function User() {
  const { user, logout } = useAuth() as IAuthProvider;
  const navigate = useNavigate();

  function handleClick() {
    logout();
    navigate('/');
  }

  return (
    <div className={styles.user}>
      <img src={user?.avatar} alt={user?.name} />
      <span>Welcome, {user?.name}</span>
      <button onClick={handleClick}>Logout</button>
    </div>
  );
}

export default User;
