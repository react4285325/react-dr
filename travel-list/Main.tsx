import * as React from "react";
import * as ReactDOM from "react-dom/client";
import "./index.css";
import App from "./components/App";
ReactDOM.createRoot(document.querySelector("#root") as HTMLInputElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
