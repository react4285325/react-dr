import Button from '../../ui/Button.tsx';
import { useDispatch } from 'react-redux';
import { decreaseItemQuantity, increaseItemQuantity } from './cartSlice.ts';

interface IProps {
  pizzaId: number;
  currentQuantity: number;
}
function UpdateItemQuantity({ pizzaId, currentQuantity }: IProps) {
  const dispatch = useDispatch();
  const handleDecreaseQuantity = () => {
    dispatch(decreaseItemQuantity(pizzaId));
  };
  const handleIncreaseQuantity = () => {
    dispatch(increaseItemQuantity(pizzaId));
  };
  return (
    <div className={'flex items-center gap-2 md:gap-3'}>
      <Button type={'round'} onClick={handleDecreaseQuantity}>
        -
      </Button>
      <span className={'bg-amber-100 px-1.5 py-1.5 text-xs font-medium'}>
        {currentQuantity}
      </span>
      <Button type={'round'} onClick={handleIncreaseQuantity}>
        +
      </Button>
    </div>
  );
}

export default UpdateItemQuantity;
