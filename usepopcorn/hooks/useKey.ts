import { useEffect } from "react";

function useKey(key: string, callback: () => void) {
  useEffect(() => {
    const escape = (e: KeyboardEvent) => {
      if (e.code.toLowerCase() === key.toLowerCase()) callback();
    };
    document.addEventListener("keydown", escape);
    return () => document.removeEventListener("keydown", escape);
  }, [callback, key]);
}
export default useKey;
