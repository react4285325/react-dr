import { ReactNode, useEffect } from 'react';
import { useAuth } from '../hooks/useAuth.ts';
import { useNavigate } from 'react-router-dom';
import { IAuthProvider } from '../context/AuthContext.tsx';

interface IProps {
  children: ReactNode;
}

function ProtectedRoute({ children }: IProps) {
  const { isAuthenticated } = useAuth() as IAuthProvider;
  const navigate = useNavigate();

  useEffect(() => {
    if (!isAuthenticated) navigate('/');
  }, [isAuthenticated, navigate]);

  return isAuthenticated ? <> {children}</> : null;
}

export default ProtectedRoute;
