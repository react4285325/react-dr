import { Component } from "react";
import { IWeathers } from "./Counter.tsx";
import Day from "./day.tsx";

interface IWeather {
  weather: IWeathers;
  location: string;
}
class Weather extends Component<IWeather> {
  render() {
    const {
      temperature_2m_max: max,
      temperature_2m_min: min,
      time: dates,
      weathercode: codes,
    } = this.props.weather;
    console.log(this.props.location);
    return (
      <div>
        <h2>{this.props.location}</h2>
        <ul className={"weather"}>
          {dates.map((date, i) => (
            <Day
              key={date}
              date={date}
              max={max.at(i)}
              min={min.at(i)}
              code={codes.at(i)}
              isToday={i === 0}
            />
          ))}
        </ul>
      </div>
    );
  }
}

export default Weather;
