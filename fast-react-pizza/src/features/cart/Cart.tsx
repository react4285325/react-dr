import LinkButton from '../../ui/LinkButton.tsx';
import Button from '../../ui/Button.tsx';
import CartItem from './CartItem.tsx';
import { useDispatch, useSelector } from 'react-redux';
import { clearCart, getCart, getUsername } from './cartSlice.ts';
import EmptyCart from './EmptyCart.tsx';

export interface ICart {
  pizzaId: number;
  name: string;
  quantity: number;
  unitPrice: number;
  totalPrice: number;
}

function Cart() {
  const dispatch = useDispatch();
  const userName = useSelector(getUsername);
  const cart = useSelector(getCart);
  const handleClearCart = () => {
    dispatch(clearCart());
  };
  if (!cart.length) return <EmptyCart />;
  return (
    <div className='px-5 py-1'>
      <LinkButton to='/menu'>&larr; Back to menu</LinkButton>

      <h2 className='mt-7 text-xl font-bold'>
        Your cart, {userName ? userName : ''}
      </h2>

      <ul className='divide--stone-300 mt-3 divide-y border-b'>
        {cart.map((item) => (
          <CartItem item={item} key={item.pizzaId} />
        ))}
      </ul>
      <div className='mt-7 space-x-3'>
        <Button type='primary' to={'/order/new'}>
          Order Pizzas
        </Button>
        <Button type={'secondary'} onClick={handleClearCart}>
          Clear Cart
        </Button>
      </div>
    </div>
  );
}

export default Cart;
