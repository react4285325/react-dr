interface IProps {
  message: string;
}
function ErrorM({ message }: IProps) {
  return <p className={"error"}>{message} 👻</p>;
}

export default ErrorM;
