import * as React from "react";
import * as ReactDom from "react-dom/client";
import "./index.css";
import App from "./App.tsx";

ReactDom.createRoot(document.querySelector("#root") as HTMLDivElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
