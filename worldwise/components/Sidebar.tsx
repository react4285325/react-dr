import style from "./Sidebar.module.css";
import AppNav from "./shared/AppNav.tsx";
import Logo from "./shared/Logo.tsx";
import Footer from "./shared/Footer.tsx";
import { Outlet } from "react-router-dom";

function Sidebar() {
  return (
    <div className={style.sidebar}>
      <Logo />
      <AppNav />
      <Outlet />
      <Footer footer={style.footer} copyright={style.copyright} />
    </div>
  );
}

export default Sidebar;
