const Pizza = ({ pizza }) => {
  return (
    <article className={"pizzas"}>
      {pizza.map((pizza) => (
        <div
          className={pizza.soldOut ? "pizza sold-out" : "pizza"}
          key={pizza.name}
          style={{ display: "flex", flexDirection: "column" }}
        >
          <img src={pizza.photoName} alt={pizza.name} />
          <div>
            <h3>{pizza.name}</h3>
            <p>{pizza.ingredients}</p>
            <span>{pizza.soldOut ? "Sold Out" : pizza.price}</span>
          </div>
        </div>
      ))}
    </article>
  );
};

export default Pizza;
