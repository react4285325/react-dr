import Spinner from '../shared/Spinner.tsx';
import CityItem from './CityItem.tsx';
import Message from '../shared/Message.tsx';
import styles from './CityList.module.css';
import useCities from '../../hooks/useCities.ts';
import { ICitiesContext } from '../../context/CititesContext.tsx';

function CityList() {
  const { cities, isLoading } = useCities() as ICitiesContext;
  if (isLoading) return <Spinner />;
  if (!cities.length)
    return (
      <Message message={'ADD your city by clicking on a city on you map'} />
    );
  return (
    <ul className={styles.cityList}>
      {cities.map((city) => (
        <CityItem city={city} key={city.id} />
      ))}
    </ul>
  );
}

export default CityList;
