import { Items } from "./App.tsx";
interface Props {
  items: Items[];
}
function Stats({ items }: Props) {
  if (!items.length)
    return <p className={"stats"}>Add some items to the list</p>;
  const numItems = items.length;
  const numPacked = items.filter((item) => item.packed).length;
  const numPercent = Math.round((numPacked / numItems) * 100);

  return (
    <footer className={"stats"}>
      <em>
        {numPercent === 100
          ? "✈️ everything set ready to go 🥳"
          : `🎒 you have ${numItems} in your list 📃, and you already packed ${numPacked} ${numPercent}% `}
      </em>
    </footer>
  );
}

export default Stats;
