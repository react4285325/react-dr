import { useParams } from 'react-router-dom';
import styles from './City.module.css';
import { useEffect } from 'react';
import useCities from '../../hooks/useCities.ts';
import { formatDate } from '../../helper/fn.ts';
import Spinner from '../shared/Spinner.tsx';
import BackButton from '../shared/BackButton.tsx';
import { ICitiesContext } from '../../context/CititesContext.tsx';
import Message from '../shared/Message.tsx';

function City() {
  const { id } = useParams();
  const { getCity, currentCity, isLoading, error } =
    useCities() as ICitiesContext;

  useEffect(() => {
    getCity(id || '');
  }, [id, getCity]);

  const { cityName, emoji, date, notes } = currentCity ?? {};
  if (isLoading) return <Spinner />;
  if (error) return <Message message={error} />;
  return (
    <div className={styles.city}>
      <div className={styles.row}>
        <h6>City name</h6>
        <h3>
          <span>{emoji}</span> {cityName}
        </h3>
      </div>

      <div className={styles.row}>
        <h6>You went to {cityName} on</h6>
        <p>{formatDate(date || null)}</p>
      </div>

      {notes && (
        <div className={styles.row}>
          <h6>Your notes</h6>
          <p>{notes}</p>
        </div>
      )}

      <div className={styles.row}>
        <h6>Learn more</h6>
        <a
          href={`https://en.wikipedia.org/wiki/${encodeURIComponent(cityName)}`}
          target='_blank'
          rel='noreferrer'
        >
          Check out {cityName} on Wikipedia &rarr;
        </a>
      </div>

      <div>
        <BackButton />
      </div>
    </div>
  );
}

export default City;
