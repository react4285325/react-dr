import { ChangeEvent, Component } from "react";
interface IProps {
  location: string;
  onChangeLocation: (e: ChangeEvent<HTMLInputElement>) => void;
}
class Input extends Component<IProps> {
  render() {
    const { location, onChangeLocation } = this.props;
    return (
      <input
        type={"text"}
        value={location}
        onChange={onChangeLocation}
        placeholder={"search for location"}
      />
    );
  }
}

export default Input;
