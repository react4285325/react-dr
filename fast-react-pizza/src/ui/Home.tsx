import CreateUser from '../features/user/CreateUser.tsx';
import { useSelector } from 'react-redux';
import Button from './Button.tsx';
import { getUsername } from '../features/cart/cartSlice.ts';

function Home() {
  const userName = useSelector(getUsername);
  return (
    <div className='my-9 text-center'>
      <h1 className='mb-8  text-2xl font-semibold md:mb-12 md:text-3xl'>
        The best pizza.
        <br />
        <span className='text-amber-500'>
          Straight out of the oven, straight to you.
        </span>
      </h1>
      {userName === '' ? (
        <CreateUser />
      ) : (
        <Button type={'primary'} to={'/menu'}>
          Go To Menu
        </Button>
      )}
    </div>
  );
}

export default Home;
