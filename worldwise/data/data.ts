export const cities = [
  {
    cityName: 'Lisbon',
    country: 'Portugal',
    emoji: '🇵🇹',
    date: '2027-10-31T15:59:59.138Z',
    notes: 'My favorite city so far!',
    position: {
      lat: 38.727881642324164,
      lng: -9.140900099907554,
    },
    id: 73930385,
  },
  {
    cityName: 'Madrid',
    country: 'Spain',
    emoji: '🇪🇸',
    date: '2027-07-15T08:22:53.976Z',
    notes: '',
    position: {
      lat: 40.46635901755316,
      lng: -3.7133789062500004,
    },
    id: 17806751,
  },
  {
    cityName: 'Berlin',
    country: 'Germany',
    emoji: '🇩🇪',
    date: '2027-02-12T09:24:11.863Z',
    notes: 'Amazing 😃',
    position: {
      lat: 52.53586782505711,
      lng: 13.376933665713324,
    },
    id: 98443197,
  },
  {
    cityName: 'Tokyo',
    country: 'Japan',
    emoji: '🇯🇵',
    date: '2028-05-01T18:45:00.000Z',
    notes: 'A vibrant and bustling metropolis.',
    position: {
      lat: 35.6895,
      lng: 139.6917,
    },
    id: 98765432,
  },
  {
    cityName: 'Paris',
    country: 'France',
    emoji: '🇫🇷',
    date: '2027-12-15T10:30:00.000Z',
    notes: 'The city of love!',
    position: {
      lat: 48.8566,
      lng: 2.3522,
    },
    id: 12345678,
  },
];
