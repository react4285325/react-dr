import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { getAddress } from '../../services/apiGeocoding.ts';
type coords = { coords: { latitude: number; longitude: number } };

function getPosition(): Promise<coords> {
  return new Promise(function (resolve, reject) {
    navigator.geolocation.getCurrentPosition(resolve, reject);
  });
}

export const fetchAddress = createAsyncThunk(
  'user/fetchAddress',
  async function () {
    // 1) We get the user's geolocation position
    const positionObj = await getPosition();
    const position = {
      latitude: positionObj.coords.latitude,
      longitude: positionObj.coords.longitude,
    };

    // 2) Then we use a reverse geocoding API to get a description of the user's address, so we can display it the order form, so that the user can correct it if wrong
    const addressObj = await getAddress(position);
    const address = `${addressObj?.locality}, ${addressObj?.city} ${addressObj?.postcode}, ${addressObj?.countryName}`;

    // 3) Then we return an object with the data that we are interested in
    return { position, address };
  }
);
export interface IUserSlice {
  username: string;
  status: 'idle' | 'loading' | 'error';
  position: { latitude: number; longitude: number };
  address: string;
  error: string;
}

const initialState: IUserSlice = {
  username: '',
  status: 'idle',
  position: { latitude: 0, longitude: 0 },
  address: '',
  error: '',
};

const userSlice = createSlice({
  initialState,
  name: 'user',
  reducers: {
    updateName(state, action) {
      state.username = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAddress.pending, (state) => {
      state.status = 'loading';
    });
    builder.addCase(fetchAddress.fulfilled, (state, action) => {
      state.status = 'idle';
      state.position = action.payload.position;
      state.address = action.payload.address;
    });
    builder.addCase(fetchAddress.rejected, (state, action) => {
      state.status = 'error';
      if (action.error.message) state.error = action.error.message;
    });
  },
});

export const { updateName } = userSlice.actions;
export default userSlice.reducer;
