import { useEffect, useRef, useState } from "react";
import { MOVIE_API_KEY } from "../../../env.local.ts";
import StarRating from "../shared/StarRating.tsx";
import Loader from "../shared/Loader.tsx";
import { IWatchData } from "../../data.ts";
import ErrorM from "../shared/ErrorM.tsx";
import useKey from "../../hooks/useKey.ts";

interface IMovie {
  Actors: string;
  Director: string;
  Genre: string;
  Plot: string;
  Poster: string;
  Released: string;
  Runtime: string;
  Title: string;
  Year: string;
  imdbRating: string;
}

interface IProps {
  selectedId: string;
  onCloseMovie: () => void;
  onAddWatched: (movie: IWatchData) => void;
  watchedMovie: IWatchData[];
}
function SelectedMovie({
  selectedId,
  onCloseMovie,
  onAddWatched,
  watchedMovie,
}: IProps) {
  const [movie, setMovie] = useState<IMovie>({
    Actors: "",
    Director: "",
    Genre: "",
    Plot: "",
    Poster: "",
    Released: "",
    Runtime: "",
    Title: "",
    Year: "",
    imdbRating: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const [userRating, setUserRating] = useState(0);
  const [error, setError] = useState("");
  const countRef = useRef(0);

  useEffect(() => {
    if (userRating) countRef.current++;
  }, [userRating]);
  const isWatched = watchedMovie
    .map((movie) => movie.imdbId)
    .includes(selectedId);

  const currentRating = watchedMovie.find(
    (movie) => movie.imdbId === selectedId
  )?.userRating;

  const {
    Title: title,
    Poster: poster,
    Runtime: runtime,
    imdbRating,
    Plot: plot,
    Released: released,
    Actors: actors,
    Director: director,
    Genre: genre,
  }: IMovie = movie;

  const handleAdd = () => {
    const newWatchedMovie = {
      imdbId: selectedId,
      title,
      runtime: +runtime.split(" ")[0],
      poster,
      imdbRating: +imdbRating,
      userRating,
      countRatingDecisions: countRef.current,
    };
    onAddWatched(newWatchedMovie);
    onCloseMovie();
  };

  useKey("Escape", onCloseMovie);

  useEffect(() => {
    const controller = new AbortController();
    (async function () {
      setError("");
      setIsLoading(true);
      try {
        const res = await fetch(
          `https://www.omdbapi.com/?apikey=${MOVIE_API_KEY}&i=${selectedId}`,
          {
            signal: controller.signal,
          }
        );
        const data = await res.json();
        setMovie(data);
      } catch (e) {
        if ((e as Error).name !== "AbortError") setError((e as Error).message);
      } finally {
        setIsLoading(false);
      }
    })();
    return () => controller.abort();
  }, [selectedId]);

  useEffect(() => {
    document.title = `📺 ${movie.Title}` || "📺 What Watch";
    return function () {
      document.title = "📺 What Watch";
    };
  }, [movie]);

  return (
    <>
      {error && <ErrorM message={error} />}
      {isLoading && <Loader />}

      {!isLoading && (
        <div className={"details"}>
          <header>
            <button className={"btn-back"} onClick={onCloseMovie}>
              &larr;
            </button>
            <img src={poster} alt={`Poster of ${title}`} />
            <div className={"details-overview"}>
              <h2>{title}</h2>

              <p>
                {released} &bull; {runtime}
              </p>
              <p>{genre}</p>
              <p>
                <span>⭐</span>
                {imdbRating}
              </p>
            </div>
          </header>
          <section>
            {!isWatched ? (
              <>
                <StarRating
                  maxRating={10}
                  defaultRating={5}
                  onSetRating={setUserRating}
                />
                {userRating > 0 && (
                  <button className={"btn-add"} onClick={handleAdd}>
                    add to wishList
                  </button>
                )}
              </>
            ) : (
              <p>Your rated this movie {currentRating}⭐ </p>
            )}
            <p>
              <em>{plot}</em>
            </p>
            <p>Starring : {actors}</p>
            <p>Directed By : {director}</p>
          </section>
        </div>
      )}
    </>
  );
}

export default SelectedMovie;
