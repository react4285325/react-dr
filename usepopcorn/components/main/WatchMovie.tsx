import { IWatchData } from "../../data.ts";

interface IProps {
  movie: IWatchData;
  onDeleteWatched: (id: string) => void;
}
function WatchMovie({ movie, onDeleteWatched }: IProps) {
  return (
    <li key={movie.imdbId}>
      <img src={movie.poster} alt={`${movie.title} poster`} />
      <h3>{movie.title}</h3>
      <div>
        <p>
          <span>⭐️</span>
          <span>{movie.imdbRating.toFixed(2)}</span>
        </p>
        <p>
          <span>🌟</span>
          <span>{movie.userRating.toFixed(2)}</span>
        </p>
        <p>
          <span>⏳</span>
          <span>{movie.runtime} min</span>
        </p>
      </div>
      <button
        className={"btn-delete"}
        onClick={() => onDeleteWatched(movie.imdbId)}
      >
        X
      </button>
    </li>
  );
}

export default WatchMovie;
