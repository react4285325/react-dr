import { createContext, ReactNode, useMemo, useState } from 'react';
import { IPost } from '../../App.tsx';
import { createRandomPost } from '../../helper.ts';

interface IPostContext {
  posts: IPost[];
  onClearPost: () => void;
  onAddPost: (post: IPost) => void;
  searchQuery: string;
  setSearchQuery: (query: string) => void;
}
const initialPostContext: IPostContext = {
  posts: [],
  onClearPost: () => {},
  onAddPost: () => {},
  searchQuery: '',
  setSearchQuery: () => {},
};

export const PostContext = createContext<IPostContext>(initialPostContext);
interface IProps {
  children: ReactNode;
}
function PostProvider({ children }: IProps) {
  const [posts, setPosts] = useState<IPost[]>(() =>
    Array.from({ length: 20 }, () => createRandomPost())
  );
  const [searchQuery, setSearchQuery] = useState('');
  // Derived state. These are the posts that will actually be displayed
  const searchedPosts =
    searchQuery.length > 0
      ? posts.filter((post) =>
          `${post.title} ${post.body}`
            .toLowerCase()
            .includes(searchQuery.toLowerCase())
        )
      : posts;
  function handleAddPost(post: IPost) {
    setPosts((posts) => [post, ...posts]);
  }

  function handleClearPosts() {
    setPosts([]);
  }
  const value = useMemo(() => {
    return {
      posts: searchedPosts,
      onClearPost: handleClearPosts,
      onAddPost: handleAddPost,
      searchQuery,
      setSearchQuery,
    };
  }, [searchedPosts, searchQuery]);
  return <PostContext.Provider value={value}>{children}</PostContext.Provider>;
}

export default PostProvider;
