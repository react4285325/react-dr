import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Home from './src/ui/Home.tsx';
import Menu, { loader as menuLoader } from './src/features/menu/Menu.tsx';
import Cart from './src/features/cart/Cart.tsx';
import CreateOrder, {
  action as createOrderAction,
} from './src/features/order/CreateOrder.tsx';
import NotFoundPage from './src/ui/NotFound.tsx';
import Order, { loader as orderLoader } from './src/features/order/Order.tsx';
import AppLayout from './src/ui/AppLayout.tsx';
import Error from './src/ui/Error.tsx';
import { action as updateOrderAction } from './src/features/order/UpdateOrder.tsx';

const router = createBrowserRouter([
  {
    element: <AppLayout />,
    errorElement: <NotFoundPage />,
    children: [
      {
        path: '/',
        element: <Home />,
      },
      {
        path: '/menu',
        element: <Menu />,
        loader: menuLoader,
        errorElement: <Error />,
      },
      {
        path: '/cart',
        element: <Cart />,
      },
      {
        path: '/order/new',
        element: <CreateOrder />,
        action: createOrderAction,
      },
      {
        path: '/order/:orderId',
        element: <Order />,
        action: updateOrderAction,
        loader: orderLoader,
        errorElement: <Error />,
      },
    ],
  },
]);
function App() {
  return <RouterProvider router={router} />;
}

export default App;
