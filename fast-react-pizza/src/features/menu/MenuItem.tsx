import { formatCurrency } from '../../utility/helpers.ts';
import { Pizza } from './Menu.tsx';
import Button from '../../ui/Button.tsx';
import { useDispatch, useSelector } from 'react-redux';
import { addItem, getCurrentQuantityById } from '../cart/cartSlice.ts';
import DeleteItem from '../cart/DeleteItem.tsx';
import UpdateItemQuantity from '../cart/UpdateItemQuantity.tsx';
interface IProps {
  pizza: Pizza;
}

function MenuItem({ pizza }: IProps) {
  const dispatch = useDispatch();
  const { name, unitPrice, ingredients, soldOut, imageUrl, id } = pizza;
  const currentQuantity = useSelector(getCurrentQuantityById(id));

  const isInCart = currentQuantity > 0;
  const handleAddToCart = () => {
    const newItem = {
      name,
      quantity: 1,
      pizzaId: id,
      unitPrice,
      totalPrice: unitPrice,
    };
    dispatch(addItem(newItem));
  };
  return (
    <li className='flex gap-4 py-2'>
      <img
        src={imageUrl}
        alt={name}
        className={`h-24 ${soldOut ? 'opacity-80 grayscale' : ''}`}
      />
      <div className={'flex grow flex-col pt-0.5'}>
        <p className='text-l font-medium'>{name}</p>
        <p className='text-sm font-light capitalize'>
          {ingredients.join(', ')}
        </p>
        <div className='mt-auto flex   items-center justify-between text-sm '>
          {!soldOut ? (
            <p>{formatCurrency(unitPrice)}</p>
          ) : (
            <p className=' font-mediu uppercase text-stone-500'>Sold out</p>
          )}
          {isInCart && (
            <>
              <UpdateItemQuantity
                pizzaId={id}
                currentQuantity={currentQuantity}
              />
              <DeleteItem pizzaId={id} />{' '}
            </>
          )}
          {!soldOut && !isInCart && (
            <Button onClick={handleAddToCart} type='small'>
              Add to Cart
            </Button>
          )}
        </div>
      </div>
    </li>
  );
}

export default MenuItem;
