import { useContext } from 'react';
import { CitiesContext } from '../context/CititesContext.tsx';

function useCities() {
  const context = useContext(CitiesContext);
  if (!context === undefined)
    throw new Error('provider called outside of context');
  return context;
}
export default useCities;
