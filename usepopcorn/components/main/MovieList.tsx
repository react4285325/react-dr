import Movie from "./Movie.tsx";
import { IMovies } from "../../data.ts";
interface IProps extends IMovies {
  onSelectMovie: (id: string) => void;
}
function MovieList({ movies, onSelectMovie }: IProps) {
  return (
    <ul className='list list-movies'>
      {movies?.map((movie) => (
        <Movie onSelectMovie={onSelectMovie} movie={movie} key={movie.imdbID} />
      ))}
    </ul>
  );
}

export default MovieList;
