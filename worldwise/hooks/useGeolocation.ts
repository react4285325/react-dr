import { useState } from 'react';

type cords = {
  lat: number;
  lng: number;
};

export function useGeolocation(defaultPosition: cords | null = null): {
  isLoading: boolean;
  position: cords | null;
  error: string | null;
  getPosition: () => void;
} {
  const [isLoading, setIsLoading] = useState(false);
  const [position, setPosition] = useState<cords | null>(defaultPosition);
  const [error, setError] = useState<string | null>(null);

  const getPosition = () => {
    if (!navigator.geolocation)
      setError('Your browser does not support geolocation');
    setIsLoading(true);

    navigator.geolocation.getCurrentPosition(
      (pos) => {
        setPosition({ lat: pos.coords.latitude, lng: pos.coords.longitude });
        setIsLoading(false);
      },
      (err) => {
        setError(err.message);
        setIsLoading(false);
      }
    );
  };

  return { isLoading, position, error, getPosition };
}
