import usePosts from './hooks/usePosts.ts';

function Results() {
  const { posts } = usePosts();
  return <p>🚀 {posts.length} atomic posts found</p>;
}
export default Results;
