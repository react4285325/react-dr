import Button from '../../ui/Button.tsx';
import { useDispatch } from 'react-redux';
import { removeItem } from './cartSlice.ts';

interface IProps {
  pizzaId: number;
}
function DeleteItem({ pizzaId }: IProps) {
  const dispatch = useDispatch();
  const handleDeleteItem = () => {
    dispatch(removeItem(pizzaId));
  };
  return (
    <Button type={'small'} onClick={handleDeleteItem}>
      Delete
    </Button>
  );
}

export default DeleteItem;
