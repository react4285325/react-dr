import Spinner from './shared/Spinner.tsx';
import styles from './SpinnerFullPage.module.css';

function SpinnerFullPage() {
  return (
    <div className={styles.spinnerFullpage}>
      <Spinner />
    </div>
  );
}

export default SpinnerFullPage;
