import { useSelector } from 'react-redux';
import { IRootState } from '../../store.ts';

function Username() {
  const user = useSelector((state: IRootState) => state.user.username);
  return <div className='hidden text-sm font-semibold md:block'>{user}</div>;
}

export default Username;
