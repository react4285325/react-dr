import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import eslintPlugin from 'vite-plugin-eslint';

export default defineConfig({
  plugins: [react(), eslintPlugin({ cache: false })],
  envDir: '/',
  appType: 'spa',
  server: {
    port: 5555,
  },
  assetsInclude: ['**/*.m4a'],
});
