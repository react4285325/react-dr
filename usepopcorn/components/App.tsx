import Navbar from "./nav/Navbar.tsx";
import Main from "./main/Main.tsx";
import { useState } from "react";
import Results from "./nav/Results.tsx";
import Search from "./nav/Search.tsx";
import MovieList from "./main/MovieList.tsx";
import Box from "./shared/Box.tsx";
import WatchedSummary from "./main/WatchedSummary.tsx";
import WatchMovieList from "./main/WatchMovieList.tsx";
import Loader from "./shared/Loader.tsx";
import ErrorM from "./shared/ErrorM.tsx";
import SelectedMovie from "./main/SelectedMovie.tsx";
import { IMovieData, IWatchData } from "../data.ts";
import useFetch from "../hooks/useFetch.ts";
import useLocalStorageState from "../hooks/useLocalStorageState.ts";

export default function App() {
  const [query, setQuery] = useState("");
  const [isLoading, error, movies]: [boolean, string, IMovieData[]] =
    useFetch(query);

  const [watched, setWatched] = useLocalStorageState<IWatchData[]>(
    [],
    "watched"
  );
  const [selectedId, setSelectedId] = useState<null | string>(null);

  const handleSelectMovie = (id: string) => {
    setSelectedId(id === selectedId ? null : id);
  };

  function handleCloseMovie() {
    setSelectedId(null);
  }

  const handleWatchedMovie = (movie: IWatchData) => {
    setWatched((watched) => [...watched, movie]);
  };

  const handleDeleteWatchedMovie = (id: string) => {
    setWatched((prevState) => prevState.filter((movie) => movie.imdbId !== id));
  };

  return (
    <>
      <Navbar>
        <Search query={query} setQuery={setQuery} />
        <Results movies={movies} />
      </Navbar>
      <Main>
        <Box>
          {isLoading && <Loader />}
          {error && <ErrorM message={error} />}
          {!isLoading && !error && (
            <MovieList onSelectMovie={handleSelectMovie} movies={movies} />
          )}
        </Box>
        <Box>
          {selectedId ? (
            <SelectedMovie
              onCloseMovie={handleCloseMovie}
              selectedId={selectedId}
              onAddWatched={handleWatchedMovie}
              watchedMovie={watched}
            />
          ) : (
            <>
              <WatchedSummary watched={watched} />
              <WatchMovieList
                watched={watched}
                onDeleteWatched={handleDeleteWatchedMovie}
              />{" "}
            </>
          )}
        </Box>
      </Main>
    </>
  );
}
