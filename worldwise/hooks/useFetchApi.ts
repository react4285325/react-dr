import { useEffect, useState } from 'react';

export function useApi(url: string): {
  isLoading: boolean;
  error: string;
  data: any;
} {
  const [data, setData] = useState(null);
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  useEffect(
    function () {
      const controller = new AbortController();
      (async function () {
        try {
          setIsLoading(true);
          setError('');
          const res = await fetch(url, { signal: controller.signal });
          if (!res.ok) throw new Error('failed to fetch data');
          const data = await res.json();
          setData(data);
        } catch (e) {
          if ((e as Error).name === 'AbortError') return;
          setError((e as Error).message);
        } finally {
          setIsLoading(false);
        }
      })();
      return () => {
        controller.abort();
      };
    },
    [url]
  );
  return { isLoading, error, data };
}
