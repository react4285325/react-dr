import { FormEvent, useState } from "react";
import { Items } from "./App.tsx";
interface Props {
  onAddItems: (items: Items) => void;
}
function Form({ onAddItems }: Props) {
  const [form, setForm] = useState({ description: "", quantity: 1 });

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    if (!form.description) return;
    const newItem = {
      description: form.description,
      quantity: form.quantity,
      packed: false,
      id: Date.now(),
    };
    onAddItems(newItem);
    setForm((prevState) => ({ ...prevState, description: "", quantity: 1 }));
  };
  return (
    <div className={"add-form"}>
      <form className={"add-form"} onSubmit={handleSubmit}>
        <h3>Track Me? </h3>
        <select
          value={form.quantity}
          onChange={(event) =>
            setForm((prevState) => ({
              ...prevState,
              quantity: +event.target.value,
            }))
          }
        >
          {Array.from({ length: 20 }, (_, i) => i + 1).map((value) => (
            <option value={value} key={value}>
              {value}
            </option>
          ))}
        </select>
        <input
          type={"text"}
          placeholder={"Item.."}
          value={form.description}
          onChange={(e) =>
            setForm((prevState) => ({
              ...prevState,
              description: e.target.value,
            }))
          }
        />
        <button>Add</button>
      </form>
    </div>
  );
}

export default Form;
