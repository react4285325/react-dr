// Test ID: IIDSAT
import {
  LoaderFunction,
  LoaderFunctionArgs,
  useFetcher,
  useLoaderData,
} from 'react-router-dom';
import {
  calcMinutesLeft,
  formatCurrency,
  formatDate,
} from '../../utility/helpers.ts';
import { getOrder } from '../../services/apiRestaurant.ts';
import OrderItem, { Item } from './OrderItem.tsx';
import { useEffect } from 'react';
import UpdateOrder from './UpdateOrder.tsx';

interface Order {
  cart: Item[];
  customer: string;
  estimatedDelivery: string;
  id: string;
  orderPrice: number;
  priority: boolean;
  priorityPrice: number;
  status: string;
}

function Order() {
  // Everyone can search for all orders, so for privacy reasons we're gonna gonna exclude names or address, these are only for the restaurant staff
  const order = useLoaderData() as Order;
  const {
    id,
    status,
    priority,
    priorityPrice,
    orderPrice,
    estimatedDelivery,
    cart,
  } = order;
  const fetcher = useFetcher();
  useEffect(() => {
    if (!fetcher.data && fetcher.state === 'idle') {
      fetcher.load('/menu');
    }
  }, [fetcher]);
  const deliveryIn = calcMinutesLeft(estimatedDelivery);
  return (
    <div className={'space-y-8 px-4 py-6'}>
      <div className={'flex flex-wrap items-center justify-between gap-2'}>
        <h2 className={'text-xl font-semibold'}>Order #{id} status</h2>
        <div className={'space-x-3'}>
          {priority && (
            <span
              className={
                'rounded bg-red-500 px-2 py-1 text-sm font-semibold uppercase tracking-wider text-stone-100'
              }
            >
              Priority
            </span>
          )}
          <span
            className={
              'rounded bg-green-500 px-2 py-1 text-sm font-semibold uppercase tracking-wider text-stone-100'
            }
          >
            {status} order
          </span>
        </div>
      </div>

      <div
        className={
          'flex flex-wrap items-center justify-between gap-2 bg-stone-200 px-3 py-5'
        }
      >
        <p className={'font-medium'}>
          {deliveryIn >= 0
            ? `Only ${calcMinutesLeft(estimatedDelivery)} minutes left 😃`
            : 'Order should have arrived'}
        </p>
        <p className={'text-xs font-light'}>
          (Estimated delivery: {formatDate(estimatedDelivery)})
        </p>
      </div>
      <ul className={'borer-b divide-y divide-stone-300 border-t'}>
        {cart.map((item) => (
          <OrderItem
            item={item}
            key={item.pizzaId}
            ingredients={
              fetcher?.data?.find((el: any) => el.id === item.pizzaId)
                .ingredients ?? []
            }
            isLoadingIngredients={fetcher.state === 'loading'}
          />
        ))}
      </ul>

      <div className={'space-y-2 bg-stone-200 px-3 py-5'}>
        <p className={'text-sm font-medium text-stone-600'}>
          Price pizza: {formatCurrency(orderPrice)}
        </p>
        {priority && <p>Price priority: {formatCurrency(priorityPrice)}</p>}
        <p className={'font-bold'}>
          To pay on delivery: {formatCurrency(orderPrice + priorityPrice)}
        </p>
      </div>
      {!priority && <UpdateOrder order={order} />}
    </div>
  );
}

type IParam = { orderId: string };
export const loader: LoaderFunction = async ({
  params,
}: LoaderFunctionArgs) => {
  const { orderId } = params as IParam;
  if (params) return await getOrder(orderId);
};
export default Order;
