interface IFormState {
  country: string;
  cityName: string;
  note: string;
  date: Date;
  emoji: string;
}
interface IFormAction {
  type: 'SET_COUNTRY' | 'SET_CITY' | 'SET_NOTE' | 'SET_EMOJI' | 'SET_DATE';
  payload: string | Date;
}

export function formReducer(
  state: IFormState,
  action: IFormAction
): IFormState {
  switch (action.type) {
    case 'SET_CITY':
      return { ...state, cityName: action.payload as string };

    case 'SET_COUNTRY':
      return { ...state, country: action.payload as string };
    case 'SET_DATE':
      return { ...state, date: action.payload as Date };
    case 'SET_EMOJI':
      return { ...state, emoji: action.payload as string };
    case 'SET_NOTE':
      return { ...state, note: action.payload as string };
    default:
      throw new Error('invalid action type 🔪');
  }
}
