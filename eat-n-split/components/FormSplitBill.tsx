import Button from "./Button.tsx";
import { IFriends } from "./App.tsx";
import { useState } from "react";

interface Props {
  selectedFriend: IFriends | null;
  onSplitBill: (value: number) => void;
}
export function FormSplitBill({ selectedFriend, onSplitBill }: Props) {
  const [form, setForm] = useState({
    bill: "",
    uExpense: "",
    fExpense: "",
    user: "user",
  });
  const paidByFriend = form.bill ? +form.bill - +form.uExpense : "";

  const handleSetForm = (e: React.ChangeEvent) => {
    const { name, value } = e.target as HTMLInputElement;

    setForm((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmitForm = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();
    if (!form.bill || !paidByFriend) return;
    onSplitBill(form.user === "user" ? paidByFriend : -paidByFriend);
  };
  return (
    <form className={"form-split-bill"}>
      <h2>Split a bill with {selectedFriend?.name}</h2>
      <label htmlFor='bill'>💷 Bill Value</label>
      <input
        id='bill'
        name={"bill"}
        type={"number"}
        value={form.bill}
        onChange={handleSetForm}
      />
      <label htmlFor='uExpense'>🪙 your Expense</label>
      <input
        id='uExpense'
        name={"uExpense"}
        type={"number"}
        value={form.uExpense}
        onChange={handleSetForm}
      />
      <label htmlFor='fExpense'>💰 {selectedFriend?.name} Expense</label>
      <input
        id='fExpense'
        name={"fExpense"}
        disabled={true}
        type={"text"}
        value={paidByFriend}
      />
      <label htmlFor='name'>💸 Who's Paying bill?</label>
      <select name={"user"} value={form.user} onChange={handleSetForm}>
        <option value={"user"}>you</option>
        <option value={"friend"}>{selectedFriend?.name}</option>
      </select>
      <Button onClick={handleSubmitForm}>Split Bill</Button>
    </form>
  );
}
