import { Component } from "react";
import { formatDay, getWeatherIcon } from "./starter.ts";

interface IDay {
  max?: number;
  min?: number;
  code?: number;
  date: string;
  isToday: boolean;
}
class Day extends Component<IDay> {
  render() {
    const { min, max, date, code, isToday } = this.props;
    return (
      <div>
        <li className={"day"}>
          <span>{getWeatherIcon(code || 0)}</span>
          <p>{isToday ? "Today" : formatDay(date)}</p>
          <p>
            {Math.floor(min ?? 0)}&deg; &mdash;{" "}
            <strong>{Math.ceil(max ?? 0)}</strong>
          </p>
        </li>
      </div>
    );
  }
}

export default Day;
