import {
  createContext,
  ReactNode,
  Reducer,
  useCallback,
  useEffect,
  useReducer,
} from 'react';
import { ICity } from '../App.tsx';
import { IAction, IState, reducer } from '../reducer/mapReducer.ts';

const URL = 'http://localhost:3000';

interface IProps {
  children: ReactNode;
}
export interface ICitiesContext {
  cities: ICity[];
  isLoading: boolean;
  currentCity: ICity;
  getCity: (id: string) => void;
  createCity: (newCity: ICity) => Promise<void>;
  deleteCity: (id: string) => Promise<void>;
  error: string;
}

const initialCurrCity = {
  cityName: '',
  id: 0,
  emoji: '',
  date: '',
  position: { lat: 0, lng: 0 },
  notes: '',
  country: '',
};
const initialState = {
  cities: [initialCurrCity],
  isLoading: false,
  currentCity: initialCurrCity,
  error: '',
};
export const CitiesContext = createContext<null | ICitiesContext>(null);

function CitiesProvider({ children }: IProps) {
  const [{ isLoading, currentCity, cities, error }, dispatch] = useReducer<
    Reducer<IState, IAction>
  >(reducer, initialState);

  useEffect(() => {
    (async function () {
      try {
        dispatch({ type: 'loading' });
        const res = await fetch(`${URL}/cities`);
        const data = await res.json();
        if (!res.ok) throw new Error('error fetching data');
        dispatch({ type: 'cities/loaded', payload: data });
      } catch {
        dispatch({ type: 'rejected', payload: 'no data found' });
      }
    })();
  }, []);

  const getCity = useCallback(
    async function (id: string) {
      if (+id === currentCity.id) return;
      try {
        dispatch({ type: 'loading' });
        const res = await fetch(`${URL}/cities/${id}`);
        const data = await res.json();
        dispatch({ type: 'city/loaded', payload: data });
      } catch {
        dispatch({
          type: 'rejected',
          payload: 'no city found for the provided id',
        });
      }
    },
    [currentCity.id]
  );
  async function createCity(newCity: ICity): Promise<void> {
    try {
      dispatch({ type: 'loading' });
      const res = await fetch(`${URL}/cities`, {
        method: 'POST',
        body: JSON.stringify(newCity),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data: ICity = await res.json();
      dispatch({ type: 'city/created', payload: data });
    } catch {
      dispatch({ type: 'rejected', payload: 'error creating the new city' });
    }
  }
  async function deleteCity(id: string): Promise<void> {
    try {
      dispatch({ type: 'loading' });
      await fetch(`${URL}/cities/${id}`, {
        method: 'DELETE',
      });
      dispatch({ type: 'cities/deleted', payload: id });
    } catch {
      dispatch({ type: 'rejected', payload: 'error when deleting city' });
    }
  }

  return (
    <CitiesContext.Provider
      value={{
        error,
        cities,
        isLoading,
        currentCity,
        getCity,
        createCity,
        deleteCity,
      }}
    >
      {children}
    </CitiesContext.Provider>
  );
}

export default CitiesProvider;
