import { Link } from 'react-router-dom';
import styles from './CityItem.module.css';
import { ICity } from '../../App.tsx';
import { formatDate } from '../../helper/fn.ts';
import useCities from '../../hooks/useCities.ts';
import { ICitiesContext } from '../../context/CititesContext.tsx';
import Message from '../shared/Message.tsx';

interface IProps {
  city: ICity;
}

function CityItem({ city }: IProps) {
  const { currentCity, deleteCity, error } = useCities() as ICitiesContext;
  const {
    cityName,
    emoji,
    date,
    id,
    position: { lat, lng },
  } = city;

  function handleClick(e: React.MouseEvent<HTMLButtonElement>) {
    e.preventDefault();
    if (id) deleteCity(id.toString());
  }
  if (error) return <Message message={error} />;
  return (
    <li>
      <Link
        className={`${styles.cityItem} ${
          currentCity.id === id ? styles['cityItem--active'] : ''
        }`}
        to={`${id}?lat=${lat}&lng=${lng}`}
      >
        <span className={styles.emoji}>{emoji}</span>
        <h3 className={styles.name}>{cityName}</h3>
        <time className={styles.date}>{formatDate(date)}</time>
        <button onClick={handleClick} className={styles.deleteBtn}>
          &times;
        </button>
      </Link>
    </li>
  );
}

export default CityItem;
