import { useContext } from 'react';
import { PostContext } from '../context/PostContext.tsx';

function usePosts() {
  const context = useContext(PostContext);
  if (context === undefined)
    throw new Error('PostContext outside the Provider');
  return context;
}
export default usePosts;
