import * as React from "react";
import * as ReactDom from "react-dom/client";
import App from "./components/App.tsx";
import "./index.css";

ReactDom.createRoot(document.querySelector("#root") as HTMLDivElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
