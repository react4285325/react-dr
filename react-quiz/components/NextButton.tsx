import { useQuiz } from '../hooks/useQuiz.ts';
import { IQuizContext } from '../context/QuizContext.tsx';

function NextButton() {
  const { dispatch, answer, index, numQuestion } = useQuiz() as IQuizContext;
  if (answer === null) return null;
  if (index < numQuestion - 1)
    return (
      <button
        className={'btn btn-ui'}
        onClick={() => dispatch({ type: 'NEXT_QUESTION' })}
      >
        Next
      </button>
    );
  return (
    <button
      className={'btn btn-ui'}
      onClick={() => dispatch({ type: 'FINISHED' })}
    >
      Finished
    </button>
  );
}

export default NextButton;
