import Header from './components/util/Header.tsx';
import Main from './components/Main.tsx';
import Error from './components/util/Error.tsx';
import Loader from './components/util/Loader.tsx';
import StartScreen from './components/StartScreen.tsx';
import Question from './components/Question.tsx';
import NextButton from './components/NextButton.tsx';
import Progress from './components/util/Progress.tsx';
import FinishScreen from './components/FinishScreen.tsx';
import Timer from './components/util/Timer.tsx';
import Footer from './components/util/Footer.tsx';
import { useQuiz } from './hooks/useQuiz.ts';
import { IQuizContext } from './context/QuizContext.tsx';

function App() {
  const {
    status,

    numQuestion,
  } = useQuiz() as IQuizContext;
  return (
    <div className={'app'}>
      <Header />
      <Main>
        {status === 'loading' && <Loader />}
        {status === 'error' && <Error />}
        {status === 'ready' && numQuestion > 0 && <StartScreen />}
        {status === 'active' && (
          <>
            <Progress />
            <Question />
            <Footer>
              <Timer />
              <NextButton />
            </Footer>
          </>
        )}
        {status === 'finished' && <FinishScreen />}
      </Main>
    </div>
  );
}

export default App;
