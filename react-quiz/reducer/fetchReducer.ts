const SEC_PER_QUESTION = 30;
export interface IQuestion {
  question: string;
  options: string[];
  correctOption: number;
  points: number;
}

export interface IQuizState {
  questions: IQuestion[];
  status: "loading" | "error" | "ready" | "active" | "finished";
  index: number;
  answer: null | number;
  points: number;
  highScore: number;
  secondsRemaining: number;
}
export interface IAction {
  type: string;
  payload?: IQuestion[] | number | null;
}

export function reducer(state: IQuizState, action: IAction): IQuizState {
  switch (action.type) {
    case "DATA_FAILED":
      return { ...state, status: "error" };
    case "DATA_RECEIVED":
      return {
        ...state,
        questions: Array.isArray(action.payload) ? action.payload : [],
        status: "ready",
      };

    case "START":
      return {
        ...state,
        status: "active",
        secondsRemaining: state.questions.length * SEC_PER_QUESTION,
      };

    case "NEW_ANSWER": {
      const currentQ = state.questions.at(state.index);
      const selectedOption =
        typeof action.payload === "number" ? action.payload : null;
      return {
        ...state,
        answer: selectedOption,
        points:
          selectedOption !== null &&
          currentQ &&
          action.payload === currentQ?.correctOption
            ? state.points + currentQ.points
            : state.points,
      };
    }
    case "NEXT_QUESTION":
      return { ...state, index: state.index + 1, answer: null };

    case "FINISHED":
      return {
        ...state,
        status: "finished",
        highScore:
          state.points > state.highScore ? state.points : state.highScore,
      };
    case "RESET":
      return {
        ...state,
        status: "ready",
        index: 0,
        questions: state.questions,
        answer: null,
        points: 0,
      };

    case "TICK":
      return {
        ...state,
        secondsRemaining: state.secondsRemaining - 1,
        status: state.secondsRemaining === 0 ? "finished" : state.status,
      };
    case "SET_HIGH_SCORE":
      return {
        ...state,
        highScore:
          (action.payload as number) > state.highScore
            ? (action.payload as number)
            : state.highScore,
        status: "ready",
      };
    default:
      throw new Error("unknown action type 🎬");
  }
}
