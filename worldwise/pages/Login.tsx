import styles from './Login.module.css';
import { useEffect, useState } from 'react';
import PageNav from '../components/shared/PageNav.tsx';
import { useAuth } from '../hooks/useAuth.ts';
import { IAuthProvider } from '../context/AuthContext.tsx';
import { useNavigate } from 'react-router-dom';
import Button from '../components/shared/Button.tsx';

export default function Login() {
  const { login, isAuthenticated } = useAuth() as IAuthProvider;
  const navigate = useNavigate();
  // PRE-FILL FOR DEV PURPOSES
  const [email, setEmail] = useState('email@nomail.com');
  const [password, setPassword] = useState('10liam');

  function handleLogin(e: React.FormEvent) {
    e.preventDefault();
    if (email && password) login(email, password);
  }

  useEffect(() => {
    if (isAuthenticated) navigate('/app', { replace: true });
  }, [isAuthenticated, navigate]);

  return (
    <main className={styles.login}>
      <PageNav />

      <form className={styles.form}>
        <div className={styles.row}>
          <label htmlFor='email'>Email address</label>
          <input
            type='email'
            id='email'
            onChange={(e) => setEmail(e.target.value)}
            value={email}
          />
        </div>

        <div className={styles.row}>
          <label htmlFor='password'>Password</label>
          <input
            type='password'
            id='password'
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />
        </div>

        <div>
          <Button onClick={handleLogin} type={'primary'}>
            Login
          </Button>
        </div>
      </form>
    </main>
  );
}
