import { useContext } from 'react';
import { QuizContext } from '../context/QuizContext.tsx';

export function useQuiz() {
  const context = useContext(QuizContext);
  if (context === undefined)
    throw new Error('quizContext called outside of Quiz Provider');
  return context;
}
