import { MouseEventHandler, ReactNode } from "react";
type Props = {
  children: ReactNode;
  onClick: MouseEventHandler<HTMLButtonElement>;
};
function Button({ children, onClick }: Props) {
  return (
    <button className={"button"} onClick={onClick}>
      {children}
    </button>
  );
}

export default Button;
