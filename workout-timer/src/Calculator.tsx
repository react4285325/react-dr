import { memo, useEffect, useState } from 'react';
import clickSound from './ClickSound.m4a';
import { IWorkouts } from './App.tsx';

interface IProps {
  workouts: IWorkouts[];
  allowSound: boolean;
}
function Calculator({ workouts, allowSound }: IProps) {
  const [number, setNumber] = useState(workouts[0].numExercises);
  const [sets, setSets] = useState(3);
  const [speed, setSpeed] = useState(90);
  const [durationBreak, setDurationBreak] = useState(5);
  const [duration, setDuration] = useState(0);

  useEffect(() => {
    setDuration((number * sets * speed) / 60 + (sets - 1) * durationBreak);
  }, [number, durationBreak, sets, speed]);

  useEffect(() => {
    const playSound = function () {
      if (!allowSound) return;
      const sound = new Audio(clickSound);
      sound.play().then();
    };
    playSound();
  }, [allowSound, duration]);

  const mins = Math.floor(duration);
  const seconds = (duration - mins) * 60;

  function handleInc() {
    setDuration((duration) => Math.floor(duration) + 1);
  }
  function handleDic() {
    setDuration((duration) => (duration > 1 ? Math.floor(duration) - 1 : 0));
  }

  return (
    <>
      <form>
        <div>
          <label htmlFor={'num'}>Type of workout</label>
          <select
            id={'num'}
            value={number}
            onChange={(e) => setNumber(+e.target.value)}
          >
            {workouts.map((workout) => (
              <option value={workout.numExercises} key={workout.name}>
                {workout.name} ({workout.numExercises} exercises)
              </option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor={'sets'}>How many sets?</label>
          <input
            id={'sets'}
            type='range'
            min='1'
            max='5'
            value={sets}
            onChange={(e) => setSets(+e.target.value)}
          />
          <span>{sets}</span>
        </div>
        <div>
          <label htmlFor={'fast'}>How fast are you?</label>
          <input
            id={'fast'}
            type='range'
            min='30'
            max='180'
            step='30'
            value={speed}
            onChange={(e) => setSpeed(+e.target.value)}
          />
          <span>{speed} sec/exercise</span>
        </div>
        <div>
          <label htmlFor={'break'}>Break length</label>
          <input
            id={'break'}
            type='range'
            min='1'
            max='10'
            value={durationBreak}
            onChange={(e) => setDurationBreak(+e.target.value)}
          />
          <span>{durationBreak} minutes/break</span>
        </div>
      </form>
      <section>
        <button onClick={handleDic}>–</button>
        <p>
          {mins < 10 && '0'}
          {mins}:{seconds < 10 && '0'}
          {seconds}
        </p>
        <button onClick={handleInc}>+</button>
      </section>
    </>
  );
}

export default memo(Calculator);
