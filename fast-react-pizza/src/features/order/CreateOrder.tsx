import {
  ActionFunction,
  ActionFunctionArgs,
  Form,
  redirect,
  useActionData,
  useNavigation,
} from 'react-router-dom';
import { createOrder } from '../../services/apiRestaurant.ts';
import Button from '../../ui/Button.tsx';
import { useDispatch, useSelector } from 'react-redux';
import {
  clearCart,
  getCart,
  getTotalCartPrice,
  getUsername,
} from '../cart/cartSlice.ts';
import EmptyCart from '../cart/EmptyCart.tsx';
import store, { IRootState } from '../../store.ts';
import { formatCurrency } from '../../utility/helpers.ts';
import { useState } from 'react';
import { fetchAddress } from '../user/userSlice.ts';

const isValidPhone = (str: string) =>
  /^\+?\d{1,4}?[-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}$/.test(
    str
  );

type errors = { phone: string };

function CreateOrder() {
  const [withPriority, setWithPriority] = useState('false');
  const {
    username,
    status: addressStatus,
    position,
    address,
    error: errorAddress,
  } = useSelector((state: IRootState) => state.user);
  const navigation = useNavigation();
  const isSubmitting = navigation.state === 'submitting';
  const formError = useActionData() as errors;
  const cart = useSelector(getCart);
  const totalCartPrice = useSelector(getTotalCartPrice);
  const priorityPrice = withPriority ? totalCartPrice * 0.2 : 0;
  const totalPrice = totalCartPrice + priorityPrice;
  const dispatch = useDispatch();
  const isLoadingAddress = addressStatus === 'loading';
  const handleLocation = (e: React.FormEvent) => {
    e.preventDefault();
    dispatch(fetchAddress());
  };
  if (!cart.length) return <EmptyCart />;
  return (
    <div className={'px-4 py-6'}>
      <h2 className={'mb-6 text-xl font-semibold'}>
        Ready to order? Let's go!
      </h2>

      <Form method='POST'>
        <div className={'mb-4 flex flex-col gap-2 sm:flex-row sm:items-center'}>
          <label htmlFor='customer' className={'sm:basis-40'}>
            First Name
          </label>
          <input
            className='input grow'
            id='customer'
            type='text'
            name='customer'
            required
            defaultValue={username}
          />
        </div>

        <div className={'mb-4 flex flex-col gap-2 sm:flex-row sm:items-center'}>
          <label htmlFor={'phone'} className={'sm:basis-40'}>
            Phone number
          </label>
          <div className={'grow'}>
            <input
              className='input w-full'
              id={'phone'}
              type='tel'
              name='phone'
              required
            />
            {formError?.phone && (
              <p className='mt-3 rounded bg-red-100 p-2 text-xs text-red-500'>
                {formError?.phone}
              </p>
            )}
          </div>
        </div>

        <div
          className={
            'relative mb-4 flex flex-col gap-2 sm:flex-row sm:items-center'
          }
        >
          <label htmlFor='address' className={'sm:basis-40'}>
            Address
          </label>
          <div className={'grow'}>
            <input
              className='input w-full'
              id='address'
              type='text'
              disabled={isLoadingAddress}
              defaultValue={address}
              name='address'
              required
            />
            {addressStatus === 'error' && (
              <p className='mt-3 rounded bg-red-100 p-2 text-xs text-red-500'>
                {errorAddress}
              </p>
            )}
          </div>
          {!position.latitude && !position.longitude && (
            <span className={'absolute right-1 z-50 '}>
              <Button
                disabled={isLoadingAddress}
                type={'small'}
                onClick={handleLocation}
              >
                Get Location
              </Button>
            </span>
          )}
        </div>

        <div className={'my-8 flex items-center gap-5'}>
          <input
            type='checkbox'
            name='priority'
            id='priority'
            className='h-5 w-5 accent-amber-400 focus:outline-none focus:ring focus:ring-amber-400 focus:ring-offset-1'
            value={withPriority}
            onChange={(e) => setWithPriority(e.target.checked)}
          />
          <label className={'font-medium'} htmlFor='priority'>
            Want to yo give your order priority?
          </label>
        </div>

        <div className={'mt-8'}>
          <input type='hidden' name='cart' value={JSON.stringify(cart)} />
          <input
            type={'hidden'}
            name='position'
            value={
              position.longitude && position.latitude
                ? JSON.stringify(position)
                : ''
            }
          />
          <Button type={'primary'} disabled={isSubmitting || isLoadingAddress}>
            {isSubmitting
              ? 'placing...order'
              : `order Now for ${formatCurrency(totalPrice)}`}
          </Button>
        </div>
      </Form>
    </div>
  );
}

export interface IOrder {
  address: string;
  cart: string;
  customer: string;
  phone: string;
  priority: boolean;
}

export const action: ActionFunction = async ({
  request,
}: ActionFunctionArgs) => {
  const formData = await request.formData();
  const data = Object.fromEntries(formData);

  const order: IOrder = {
    ...data,
    cart: JSON.parse(data.cart as string),
    priority: data.priority === 'true',
  };
  const errors: errors = { phone: '' };
  if (!isValidPhone(order.phone)) {
    errors.phone = 'Please give a correct phone number';
  }
  if (Object.values(errors).some((item) => item !== '')) {
    return errors;
  }

  const newOrder = await createOrder(order);
  // danger
  store.dispatch(clearCart());

  return redirect(`/order/${newOrder.id}`);
};

export default CreateOrder;
