import Options from './Options.tsx';
import { useQuiz } from '../hooks/useQuiz.ts';
import { IQuizContext } from '../context/QuizContext.tsx';

function Question() {
  const { questions, index } = useQuiz() as IQuizContext;
  return (
    <div>
      <h4>{questions[index].question}</h4>
      <Options />
    </div>
  );
}

export default Question;
