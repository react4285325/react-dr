import { ReactNode } from "react";

interface Props {
  children: ReactNode;
  step: number;
}

function Message({ step, children }: Props) {
  return (
    <>
      <div className={"message"}>
        <h3>step {step}:</h3>
        {children}
      </div>
    </>
  );
}

export default Message;
