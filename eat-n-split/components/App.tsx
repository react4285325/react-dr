import FriendsList from "./FriendsList.tsx";
import FormAddFriend from "./FormAddFriend.tsx";
import Button from "./Button.tsx";
import { FormSplitBill } from "./FormSplitBill.tsx";
import { useState } from "react";

const initial = [
  {
    id: 118836,
    name: "Clark",
    image: "https://i.pravatar.cc/48?u=118836",
    balance: -7,
  },
  {
    id: 933372,
    name: "Sarah",
    image: "https://i.pravatar.cc/48?u=933372",
    balance: 20,
  },
  {
    id: 499476,
    name: "Anthony",
    image: "https://i.pravatar.cc/48?u=499476",
    balance: 0,
  },
];

export interface IFriends {
  balance: number;
  id: number;
  image: string;
  name: string;
}

function App() {
  const [initialFriends, setInitialFriends] = useState(initial);
  const [showAddFriend, setShowAddFriend] = useState(false);
  const [selectedFriend, setSelectedFriend] = useState<IFriends | null>(null);

  const handleAddFriend = () => setShowAddFriend((prevState) => !prevState);

  const handleSelectFriend = (friend: IFriends) => {
    setSelectedFriend((prevState) =>
      prevState?.id === friend.id ? null : friend
    );
    setShowAddFriend(false);
  };

  const handleSplitBill = (value: number) => {
    setInitialFriends((fr) =>
      fr.map((f) =>
        f.id === selectedFriend?.id ? { ...f, balance: f.balance + value } : f
      )
    );
    setSelectedFriend(null);
  };

  return (
    <div className={"app"}>
      <div className={"sidebar"}>
        <FriendsList
          friends={initialFriends}
          onSelectFriend={handleSelectFriend}
          selectedFriend={selectedFriend}
        />
        {showAddFriend && <FormAddFriend />}
        <Button onClick={handleAddFriend}>
          {showAddFriend ? "Close" : "Add Friend"}
        </Button>
      </div>
      {selectedFriend && (
        <FormSplitBill
          key={selectedFriend.id}
          onSplitBill={handleSplitBill}
          selectedFriend={selectedFriend}
        />
      )}
    </div>
  );
}

export default App;
