import { getMenu } from '../../services/apiRestaurant.ts';
import { useLoaderData } from 'react-router-dom';
import MenuItem from './MenuItem.tsx';
export interface Pizza {
  pizzaId: number;
  name: string;
  unitPrice: number;
  imageUrl: string;
  ingredients: string[];
  soldOut: boolean;
}

function Menu() {
  const menu = useLoaderData() as Pizza[];
  if (!menu) return <p>no pizza found</p>;
  return (
    <ul className='divide-y  divide-stone-300 px-3'>
      {menu.map((pizza) => (
        <MenuItem key={pizza.pizzaId} pizza={pizza} />
      ))}
    </ul>
  );
}

export async function loader() {
  return await getMenu();
}

export default Menu;
