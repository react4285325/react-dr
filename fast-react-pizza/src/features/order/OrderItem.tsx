import { formatCurrency } from '../../utility/helpers.ts';
export interface Item {
  name: string;
  pizzaId: number;
  quantity: number;
  totalPrice: number;
  unitPrice: number;
}
interface IProps {
  item: Item;
  isLoadingIngredients: boolean;
  ingredients: string[];
}

function OrderItem({ item, isLoadingIngredients, ingredients }: IProps) {
  const { quantity, name, totalPrice } = item;

  return (
    <li className={'py-2'}>
      <div className={'flex items-center justify-between gap-4 text-sm'}>
        <p>
          <span className={'font-bold'}>{quantity}&times;</span> {name}
        </p>
        <p className={'font-bold'}>{formatCurrency(totalPrice)}</p>
      </div>
      <p className='text-sm capitalize italic text-stone-500'>
        {!isLoadingIngredients && ingredients
          ? ingredients.join(', ')
          : 'loading..'}
      </p>
    </li>
  );
}

export default OrderItem;
