import {
  createContext,
  ReactNode,
  Reducer,
  useEffect,
  useReducer,
} from 'react';
import {
  IAction,
  IQuestion,
  IQuizState,
  reducer,
} from '../reducer/fetchReducer.ts';

export interface IQuizContext extends IQuizState {
  dispatch: (action: IAction) => void;
  numQuestion: number;
  maxPoint: number;
}
interface IProps {
  children: ReactNode;
}
const initialQuestionState: IQuestion = {
  question: '',
  options: [''],
  correctOption: 0,
  points: 0,
};

const initialState: IQuizState = {
  questions: [initialQuestionState],
  status: 'loading',
  index: 0,
  answer: null,
  points: 0,
  highScore: 0,
  secondsRemaining: 0,
};
export const QuizContext = createContext<IQuizContext | null>(null);
export function QuizProvider({ children }: IProps) {
  const [
    { index, points, highScore, secondsRemaining, answer, status, questions },
    dispatch,
  ] = useReducer<Reducer<IQuizState, IAction>>(reducer, initialState);

  useEffect(() => {
    fetch('http://localhost:3000/questions')
      .then((r) => r.json())
      .then((dat) => {
        dispatch({ type: 'DATA_RECEIVED', payload: dat });
      })
      .catch(() => dispatch({ type: 'DATA_FAILED' }));
  }, []);

  useEffect(() => {
    const score = localStorage.getItem('highScore') || 0;
    dispatch({ type: 'SET_HIGH_SCORE', payload: +score });
  }, []);

  useEffect(() => {
    localStorage.setItem('highScore', highScore.toString());
  }, [highScore]);

  const numQuestion = questions.length;
  const maxPoint = questions.reduce((acc, cur) => acc + cur.points, 0);
  return (
    <QuizContext.Provider
      value={{
        index,
        points,
        highScore,
        status,
        secondsRemaining,
        questions,
        answer,
        dispatch,
        numQuestion,
        maxPoint,
      }}
    >
      {children}
    </QuizContext.Provider>
  );
}
