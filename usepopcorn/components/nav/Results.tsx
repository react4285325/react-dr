import { IMovies } from "../../data.ts";

function Results({ movies }: IMovies) {
  return (
    <p className='num-results'>
      Found <strong>{movies.length}</strong> results
    </p>
  );
}

export default Results;
