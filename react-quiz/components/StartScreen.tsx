import { useQuiz } from '../hooks/useQuiz.ts';
import { IQuizContext } from '../context/QuizContext.tsx';

function StartScreen() {
  const { dispatch, numQuestion } = useQuiz() as IQuizContext;
  return (
    <div>
      <h2>welcome to the react Quiz</h2>
      <h3>{numQuestion} Questions test your skills in react</h3>
      <button
        className={'btn btn-ui'}
        onClick={() => dispatch({ type: 'START' })}
      >
        Let's Start
      </button>
    </div>
  );
}

export default StartScreen;
