import { useState } from "react";
import Button from "./Button.tsx";
import Message from "./Message.tsx";

const messages = [
  "Learn React ⚛️",
  "Apply for jobs 💼",
  "Invest your new income 🤑",
];
function App() {
  const [step, setStep] = useState(1);
  const [isOpen, setIsOpen] = useState(true);
  const handleNext = () => {
    if (step > 2) return;
    setStep((prevState) => prevState + 1);
  };

  const handlePrev = () => {
    if (step <= 1) return;
    setStep((prevState) => prevState - 1);
  };
  const handleClose = () => setIsOpen((prevState) => !prevState);
  return (
    <>
      <button className={"close"} onClick={handleClose}>
        &times;
      </button>
      {isOpen && (
        <div className={"steps"}>
          <div className={"numbers"}>
            <div className={`${step >= 1 ? "active" : ""}`}>1</div>
            <div className={`${step >= 2 ? "active" : ""}`}>2</div>
            <div className={`${step >= 3 ? "active" : ""}`}>3</div>
          </div>
          <Message step={step}>{messages[step - 1]}</Message>
          <div className={"buttons"}>
            <Button
              color={"white"}
              bgColor={"#7950f1"}
              clickHandler={handlePrev}
            >
              &lt; Prev
            </Button>
            <Button
              color={"white"}
              bgColor={"#7950f1"}
              clickHandler={handleNext}
            >
              Next &gt;
            </Button>
          </div>
        </div>
      )}
    </>
  );
}

export default App;
