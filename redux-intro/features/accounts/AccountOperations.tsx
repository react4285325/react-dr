import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deposit, payLoan, requestLoan, withdraw } from './acconuntSlice.ts';
import { IRootState } from '../../store.ts';

type amount = number | string;
function AccountOperations() {
  const [depositAmount, setDepositAmount] = useState<amount>('');
  const [withdrawalAmount, setWithdrawalAmount] = useState<amount>('');
  const [loanAmount, setLoanAmount] = useState<amount>('');
  const [loanPurpose, setLoanPurpose] = useState('');
  const [currency, setCurrency] = useState('USD');
  const {
    loan: currentLoan,
    loanPurpose: currentLoanPurpose,
    isLoading,
  } = useSelector((store: IRootState) => store.account);
  const dispatch = useDispatch();

  function handleDeposit() {
    if (!+depositAmount || +depositAmount <= 0) return;
    dispatch(deposit(+depositAmount, currency));
    setDepositAmount('');
    setCurrency('USD');
  }

  function handleWithdrawal() {
    if (!+withdrawalAmount || +withdrawalAmount <= 0) return;
    dispatch(withdraw({ amount: +withdrawalAmount }));
    setWithdrawalAmount('');
  }

  function handleRequestLoan() {
    if (!+loanAmount || +loanAmount <= 0 || loanPurpose.length < 3) return;
    dispatch(requestLoan({ loanPurpose, amount: +loanAmount }));
    setLoanAmount('');
    setLoanPurpose('');
  }

  function handlePayLoan() {
    if (!currentLoan) return;
    dispatch(payLoan());
  }

  return (
    <div>
      <h2>Your account operations</h2>
      <div className='inputs'>
        <div>
          <label htmlFor='depost'>Deposit</label>
          <input
            id='deposit'
            type='number'
            value={depositAmount}
            onChange={(e) => setDepositAmount(+e.target.value)}
          />
          <select
            value={currency}
            onChange={(e) => setCurrency(e.target.value)}
          >
            <option value='USD'>US Dollar</option>
            <option value='EUR'>Euro</option>
            <option value='GBP'>British Pound</option>
          </select>

          <button onClick={handleDeposit} disabled={isLoading}>
            {isLoading ? 'converting..' : `Deposit ${depositAmount}`}
          </button>
        </div>

        <div>
          <label htmlFor='withdraw'>Withdraw</label>
          <input
            type='number'
            id='withdraw'
            value={withdrawalAmount}
            onChange={(e) => setWithdrawalAmount(+e.target.value)}
          />
          <button onClick={handleWithdrawal}>
            Withdraw {withdrawalAmount}
          </button>
        </div>

        <div>
          <label htmlFor='loadn'>Request loan</label>
          <input
            type='number'
            id='loan'
            value={loanAmount}
            onChange={(e) => setLoanAmount(+e.target.value)}
            placeholder='Loan amount'
          />
          <input
            value={loanPurpose}
            onChange={(e) => setLoanPurpose(e.target.value)}
            placeholder='Loan purpose'
          />
          <button onClick={handleRequestLoan}>Request loan</button>
        </div>
        {currentLoan > 0 && (
          <div>
            <span>
              Pay back ${currentLoan}({currentLoanPurpose})
            </span>
            <button onClick={handlePayLoan}> Pay loan</button>
          </div>
        )}
      </div>
    </div>
  );
}

export default AccountOperations;
