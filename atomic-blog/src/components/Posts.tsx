import List from './List.tsx';

function Posts() {
  return (
    <section>
      <List />
    </section>
  );
}

export default Posts;
