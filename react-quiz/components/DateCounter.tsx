import { ChangeEvent, Reducer, useReducer } from "react";

const initialState = { count: 0, step: 1 };

interface State {
  count: number;
  step: number;
}

interface Action {
  type: string;
  payload?: number;
}
function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "INC":
      return { ...state, count: state.count + state.step };
    case "DEC":
      return { ...state, count: state.count - state.step };
    case "SET_COUNT":
      return { ...state, count: action.payload || state.count };
    case "SET_STEP":
      return { ...state, step: action.payload || 0 };
    case "RESET":
      return { count: 0, step: 0 };
    default:
      throw new Error("Unknown action");
  }
}

function DateCounter() {
  const [state, dispatch] = useReducer<Reducer<State, Action>>(
    reducer,
    initialState
  );
  const { count, step } = state;

  // This mutates the date object.
  const date = new Date("june 21 2027");
  date.setDate(date.getDate() + count);

  const dec = function () {
    dispatch({ type: "DEC", payload: 1 });
  };

  const inc = function () {
    dispatch({ type: "INC", payload: 1 });
  };

  const defineCount = function (e: ChangeEvent<HTMLInputElement>) {
    dispatch({ type: "SET_COUNT", payload: +e.target.value });
  };

  const defineStep = function (e: ChangeEvent<HTMLInputElement>) {
    dispatch({ type: "SET_STEP", payload: +e.target.value });
  };

  const reset = function () {
    dispatch({ type: "RESET" });
  };

  return (
    <div className='counter'>
      <div>
        <input
          type='range'
          min='0'
          max='10'
          value={step}
          onChange={defineStep}
        />
        <span>{step}</span>
      </div>

      <div>
        <button onClick={dec}>-</button>
        <input value={count} onChange={defineCount} />
        <button onClick={inc}>+</button>
      </div>

      <p>{date.toDateString()}</p>

      <div>
        <button onClick={reset}>Reset</button>
      </div>
    </div>
  );
}
export default DateCounter;
